<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('command:clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('optimize:clear');
    Artisan::call('view:clear');
    Artisan::call('config:cache');
    return "config, cache, and view cleared successfully";
});

Route::get('command:config', function() {
    Artisan::call('config:cache');
    return "config cache successfully";
});

Route::get('command:key', function() {
    Artisan::call('key:generate');
    return "Key generate successfully";
});

Route::get('command:migrate', function() {
    Artisan::call('migrate:refresh');
    return "Database migration generated";
});

Route::get('command:seed', function() {
    Artisan::call('db:seed');
    return "Database seeding generated";
});



Route::group(['middleware' => ['prevent-back-history', 'mail-service']], function(){
    Route::group(['middleware' => ['guest']], function () {
        Route::get('/', 'AuthController@login')->name('login');
        Route::post('signin', 'AuthController@signin')->name('signin');

        Route::get('forgot-password', 'AuthController@forgot_password')->name('forgot.password');
        Route::post('password-forgot', 'AuthController@password_forgot')->name('password.forgot');
        Route::get('reset-password/{string}', 'AuthController@reset_password')->name('reset.password');
        Route::post('recover-password', 'AuthController@recover_password')->name('recover.password');
    });

    Route::group(['middleware' => ['auth']], function () {
        Route::get('logout', 'AuthController@logout')->name('logout');

        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
        /** access control */
            /** role */
                Route::any('role', 'RoleController@index')->name('role');
                Route::get('role/create', 'RoleController@create')->name('role.create');
                Route::post('role/insert', 'RoleController@insert')->name('role.insert');
                Route::get('role/edit', 'RoleController@edit')->name('role.edit');
                Route::patch('role/update/{id?}', 'RoleController@update')->name('role.update');
                Route::get('role/view', 'RoleController@view')->name('role.view');
                Route::post('role/delete', 'RoleController@delete')->name('role.delete');
            /** role */

            /** permission */
                Route::any('permission', 'PermissionController@index')->name('permission');
                Route::get('permission/create', 'PermissionController@create')->name('permission.create');
                Route::post('permission/insert', 'PermissionController@insert')->name('permission.insert');
                Route::get('permission/edit', 'PermissionController@edit')->name('permission.edit');
                Route::patch('permission/update/{id?}', 'PermissionController@update')->name('permission.update');
                Route::get('permission/view', 'PermissionController@view')->name('permission.view');
                Route::post('permission/delete', 'PermissionController@delete')->name('permission.delete');
            /** permission */

            /** access */
                Route::any('access_permission', 'AccessController@index')->name('access_permission');
                Route::get('access_permission/edit', 'AccessController@edit')->name('access_permission.edit');
                Route::patch('access_permission/update/{id?}', 'AccessController@update')->name('access_permission.update');
                Route::get('access_permission/view', 'AccessController@view')->name('access_permission.view');
            /** access */
        /** access control */

        /** users */
            Route::any('user', 'UserController@index')->name('user');
            Route::get('user/create', 'UserController@create')->name('user.create');
            Route::post('user/insert', 'UserController@insert')->name('user.insert');
            Route::get('user/view/{id?}', 'UserController@view')->name('user.view');
            Route::get('user/edit/{id?}', 'UserController@edit')->name('user.edit');
            Route::any('user/trash', 'UserController@Trash')->name('user.trash');
            Route::patch('user/update', 'UserController@update')->name('user.update');
            Route::post('user/change-status', 'UserController@change_status')->name('user.change.status');
            Route::post('users/exel_import','UserController@exel_import')->name('user.exel_import');
            Route::post('users/import','UserController@import')->name('user.import');
            Route::get('users/export/{slug?}','UserController@export')->name('user.export');

            Route::post('user/permenant-remove', 'UserController@Permenant_delete')->name('user.permenant.remove');
        /** users */

      
           
               /** Golf ground region */
            Route::any('plan', 'planController@index')->name('plan');
            Route::get('plan/create', 'planController@create')->name('plan.create');
            Route::post('plan/insert', 'planController@insert')->name('plan.insert');
            Route::get('plan/view/{id?}', 'planController@view')->name('plan.view');
            Route::get('plan/edit/{id?}', 'planController@edit')->name('plan.edit');
            Route::patch('plan/update', 'planController@update')->name('plan.update');
            Route::post('plan/change-status', 'planController@change_status')->name('plan.change.status');
            Route::get('plan/export/{slug?}','planController@export')->name('plan.export');
            route::post('plan/import','planController@import')->name('plan.import');
        /** region*/


            /** service_master */
            Route::any('course', 'CourseController@index')->name('course');
            Route::get('course/create', 'CourseController@create')->name('course.create');
            Route::post('course/insert', 'CourseController@insert')->name('course.insert');
            Route::get('course/view/{id?}', 'CourseController@view')->name('course.view');
            Route::get('course/edit/{id?}', 'CourseController@edit')->name('course.edit');
            Route::patch('course/update', 'CourseController@update')->name('course.update');
            Route::post('course/change-status', 'CourseController@change_status')->name('course.change.status');
            Route::get('course/export/{slug?}','CourseController@export')->name('course.export');
            route::post('course/import','CourseController@import')->name('course.import');
            

            Route::any('tee','TeeMasterController@index')->name('tee');
            Route::get('tee/create', 'TeeMasterController@create')->name('tee.create');
            Route::post('tee/insert', 'TeeMasterController@insert')->name('tee.insert');
            Route::get('tee/view/{id?}', 'TeeMasterController@view')->name('tee.view');
            Route::get('tee/edit/{id?}', 'TeeMasterController@edit')->name('tee.edit');
            Route::patch('tee/update', 'TeeMasterController@update')->name('tee.update');
            Route::post('tee/change-status', 'TeeMasterController@change_status')->name('tee.change.status');


            Route::any('member', 'membercontroller@index')->name('member');
            Route::get('member/create', 'membercontroller@create')->name('member.create');
            Route::post('member/insert', 'membercontroller@insert')->name('member.insert');
            Route::get('member/view/{id?}', 'membercontroller@view')->name('member.view');
            Route::get('member/edit/{id?}', 'membercontroller@edit')->name('member.edit');
            Route::patch('member/update', 'membercontroller@update')->name('member.update');
            Route::post('member/change-status', 'membercontroller@change_status')->name('member.change.status');
            Route::get('member/export/{slug?}','memberController@export')->name('member.export');
            route::post('member/import','membercontroller@import')->name('member.import');


           
            Route::any('clubs','ClubMasterController@index')->name('clubs');
            Route::get('clubs/create', 'ClubMasterController@create')->name('clubs.create');
            Route::post('clubs/insert', 'ClubMasterController@insert')->name('clubs.insert');
            Route::get('clubs/view/{id?}', 'ClubMasterController@view')->name('clubs.view');
            Route::get('clubs/edit/{id?}', 'ClubMasterController@edit')->name('clubs.edit');
            Route::patch('clubs/update', 'ClubMasterController@update')->name('clubs.update');
            Route::post('clubs/change-status', 'ClubMasterController@change_status')->name('clubs.change.status');
            Route::get('clubs/export/{slug?}','ClubMasterController@export')->name('clubs.export');
            Route::post('clubs/import','ClubMasterController@import')->name('clubs.import');
 

            
        /** Supplier */

            Route::any('user_master', 'UserMasterController@index')->name('user_master');
            Route::get('user_master/create', 'UserMasterController@create')->name('user_master.create');
            Route::post('user_master/insert', 'UserMasterController@insert')->name('user_master.insert');
            Route::get('user_master/view/{id?}', 'UserMasterController@view')->name('user_master.view');
            Route::get('user_master/edit/{id?}', 'UserMasterController@edit')->name('user_master.edit');
            Route::patch('user_master/update', 'UserMasterController@update')->name('user_master.update');
            Route::post('user_master/change-status', 'UserMasterController@change_status')->name('user_master.change.status');
            Route::get('user_master/export/{slug?}','UserMasterController@export')->name('user_master.export');
            Route::get('user_master/import/{slug?}','UserMasterController@import')->name('user_master.import');
            /** user_master */
            
            Route::any('payment', 'PaymentController@index')->name('payment');
            Route::get('payment/view/{id?}', 'PaymentController@view')->name('payment.view');
            Route::post('payment/change-status', 'PaymentController@change_status')->name('payment.change.status');
       
            Route::any('round', 'RoundController@index')->name('round');
            Route::get('round/view/{id?}', 'RoundController@view')->name('round.view');
            Route::post('round/change-status', 'RoundController@change_status')->name('round.change.status');
     

    });


});
