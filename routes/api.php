<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
| 
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'namespace' => 'API'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('memberlogin', 'AuthController@memberlogin');
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::get('logout', 'AuthController@logout');

        /** permission */
            Route::get('permission', 'PermissionController@index');
            Route::post('permission/insert', 'PermissionController@insert');
            Route::get('permission/view/{id?}', 'PermissionController@view');
            Route::post('permission/update', 'PermissionController@update');
            Route::post('permission/change-status', 'PermissionController@change_status');
        /** permission */

        /** Roles */
            Route::get('role', 'RolesController@index');
            Route::post('role/insert', 'RolesController@insert');
            Route::get('role/view/{id?}', 'RolesController@view');
            Route::post('role/update', 'RolesController@update');
            Route::post('role/change-status', 'RolesController@change_status');
        /** Roles */

        /** Roles Has Permission */
            Route::get('access', 'AccessController@index');
            Route::post('access/insert', 'AccessController@insert');
            Route::get('access/view/{id?}', 'AccessController@view');
            Route::post('access/update', 'AccessController@update');
        /** Roles Has Permission */
       
        /** users */
            Route::get('users', 'UsersController@index');
            Route::post('users/insert', 'UsersController@insert');
            Route::get('users/view/{id?}', 'UsersController@view');
            Route::post('users/update', 'UsersController@update');
            Route::post('users/change-status', 'UsersController@change_status');
            Route::post('users/import','UsersController@import');
            Route::post('users/export','UsersController@export');
        /** users */
       
             
    });



    
    Route::get('memberlist', 'MemberController@index');


    Route::post('customer/login', 'MemberController@login');
    Route::post('customer/login', 'MemberController@login');
    Route::post('member/login', 'MemberController@login');
    Route::post('member/insert', 'MemberController@insert');
    Route::post('member/changepass', 'MemberController@changepass');
    
    Route::any('member/token', 'MemberController@token');
    Route::any('member/tokenverify', 'MemberController@tokenverify');
    Route::any('member/sociallogin', 'MemberController@sociallogin');


    Route::get('golf/clublist', 'GolfController@clublist');
    Route::any('golf/plan', 'GolfController@plan');
    Route::post('golf/zipcode', 'GolfController@zipcode');
    Route::post('golf/latlong', 'GolfController@latlong');
    Route::post('golf/updateprofileimage', 'GolfController@updateprofileimage');

    Route::post('golf/updatepassword', 'GolfController@updatepassword');

    Route::post('golf/user_club', 'GolfController@user_club');
    Route::post('golf/user_club_save', 'GolfController@user_club_save');

    Route::post('golf/user_plan_purchase', 'GolfController@user_plan_purchase');
    Route::post('golf/user_course_save', 'GolfController@user_course_save');

    Route::post('golf/search_member', 'GolfController@search_member');
    Route::post('golf/add_guest', 'GolfController@add_guest');

    Route::post('golf/add_comment', 'GolfController@add_comment');
    Route::post('golf/add_round', 'GolfController@add_round');
    
    Route::post('golf/my_round', 'GolfController@my_round');
    Route::post('golf/commentlist', 'GolfController@commentlist');
    Route::post('golf/add_rating', 'GolfController@add_rating');
    Route::get('golf/tee_master', 'GolfController@tee_master');


    Route::post('golf/add_scrore', 'GolfController@add_scrore');
    Route::post('golf/list_scrore', 'GolfController@list_scrore');

    Route::post('golf/add_guest_scrore', 'GolfController@add_guest_scrore');
    
    Route::post('golf/course_detail', 'GolfController@course_detail');

    Route::post('golf/send_mail', 'GolfController@send_mail');


});

Route::get('/unauthenticated', function () {
    return response()->json(['status' => 201, 'message' => 'Unauthorized Access']);
})->name('api.unauthenticated');

Route::get("{path}", function(){ return response()->json(['status' => 500, 'message' => 'Bad request']); })->where('path', '.+');



