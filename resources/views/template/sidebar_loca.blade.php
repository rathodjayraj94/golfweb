<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('dashboard') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">{{ auth()->user()->first_name }}  {{ auth()->user()->last_name }}</div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item {{ Request::is('dashboard*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <hr class="sidebar-divider">
    @canany(['role-create', 'role-edit', 'role-view', 'role-delete', 'permission-create', 'permission-edit', 'permission-view', 'permission-delete', 'access-edit', 'access-view'])
        <!-- <div class="sidebar-heading">
            Access Control
        </div> -->
        <li class="nav-item {{ (Request::is('role*') || Request::is('permission*') || Request::is('access_permission*')) ? 'active' : '' }}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                aria-expanded="false" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Access Control</span>
            </a>
            <div id="collapseTwo" class="collapse {{ (Request::is('role*') || Request::is('permission*') || Request::is('access_permission*')) ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    @canany(['role-create', 'role-edit', 'role-view', 'role-delete'])
                        <a class="collapse-item {{ Request::is('role*') ? 'active' : '' }}" href="{{ route('role') }}">Roles</a>
                    @endcanany
                    @canany(['permission-create', 'permission-edit', 'permission-view', 'permission-delete'])
                        <a class="collapse-item {{ Request::is('permission*') ? 'active' : '' }}" href="{{ route('permission') }}">Permission</a>
                    @endcanany
                    @canany(['access-edit', 'access-view'])
                        <a class="collapse-item {{ Request::is('access_permission*') ? 'active' : '' }}" href="{{ route('access_permission') }}">Access</a>
                    @endcanany
                </div>
            </div>
        </li>
    @endcanany
<!-- Access Control -->
<!-- User -->
    @canany(['user-create','user-edit','user-view','user-delete'])
    <li class="nav-item {{ Request::is('user*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('user') }}">
            <i class="fa fa-users"></i>
            <span>Users</span>
        </a>
    </li>
    @endcanany
<!-- User -->



<!-- OBF -->
    @canany(['obf-create', 'obf-edit', 'obf-view', 'obf-delete', 'obf_approval-create', 'obf_approval-edit', 'obf_approval-view', 'obf_approval-delete', 'obf-rtoview', 'obf-insuranceview', 'obf-fasttagview'])
        <li class="nav-item {{ (Request::is('obf*') || Request::is('obf_approval*') || Request::is('cash_receipt*')) ? 'active' : '' }}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne"
                aria-expanded="false" aria-controls="collapseOne">
                <i class="fa fa-file" aria-hidden="true"></i>
                <span>ORDERS</span>
            </a>
            <div id="collapseOne" class="collapse {{ (Request::is('obf*') || Request::is('obf_approval*') || Request::is('cash_receipt*')) ? 'show' : '' }}" aria-labelledby="headingOne" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    @canany(['obf-create', 'obf-edit', 'obf-view', 'obf-delete','obf-fasttagview','obf-rtoview','obf-insuranceview'])
                        <a class="collapse-item {{ Request::is('obf*') ? 'active' : '' }}" href="{{ route('obf') }}">ORDERS</a>
                    @endcanany
                </div>
            </div>
        </li>
    @endcanany
<!-- OBF -->

<!-- Masters -->
@canany(['orders-create','orders-edit','orders-view','orders-delete','transfer-create','transfer-edit','transfer-view','transfer-delete','products-create','products-edit','products-view','products-delete','taxes-create','taxes-edit','taxes-view','taxes-delete','insurance-create','insurance-edit','insurance-view','insurance-delete','extand_warranties-create','extand_warranties-edit','extand_warranties-view','extand_warranties-delete','fasttags-create','fasttags-edit','fasttags-view','fasttags-delete','finance-create','finance-edit','finance-view','finance-delete','branches-create','branches-edit','branches-view','branches-delete','department-create','department-edit','department-view','department-delete','lead-create','lead-edit','lead-view','lead-delete' ,'accessories-create','accessories-edit','accessories-view','accessories-delete'])
        <li class="nav-item {{ (Request::is('products*') || Request::is('tax*') || Request::is('insurance*') || Request::is('brands*') || Request::is('fasttag*') || Request::is('finance.*') || Request::is('branches*') || Request::is('payment*') || Request::is('department*') ||  Request::is('lead*') ||  Request::is('accessory*') ||  Request::is('asset_master*') ||  Request::is('categories_master*') || Request::is('sub_categories_master*') || Request::is('donation_type_master*') || Request::is('payment_mode_master*') || Request::is('bank_account_master*') || Request::is('product_with_inventory*') || Request::is('product_type_master*') || Request::is('unit_master*') || Request::is('vendor_type_master*') || Request::is('supplier_master*') || Request::is('designation_master*') || Request::is('department_master*') || Request::is('books_master*') || Request::is('author_master*') || Request::is('book_rack_master*') || Request::is('donor_Type_master*') || Request::is('donation_purpose_master*')||Request::is('location_of_donation*') || Request::is('donation_activities_master*')||Request::is('room_status_master*')|| Request::is('maintenance_activities_master*')||Request::is('charges_master*')|| Request::is('complaints_types_master*')|| Request::is('book_rack_details*')|| Request::is('user_master*')  ) ? 'active' : '' }}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree"
                aria-expanded="false" aria-controls="collapseThree">
                <i class="fas fa-asterisk"></i>
                <span>Masters</span>
            </a>
            <div id="collapseThree" class="collapse {{ (Request::is('accessory*')) ? 'show' : '' }}" aria-labelledby="headingOne" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item {{ Request::is('country*') ? 'active' : '' }}" href="{{ route('country') }}">Country</a>
                        <a class="collapse-item {{ Request::is('state*') ? 'active' : '' }}" href="{{ route('state') }}">State</a>
                        <a class="collapse-item {{ Request::is('city*') ? 'active' : '' }}" href="{{ route('city') }}">City</a>
                        <a class="collapse-item {{ Request::is('kyc*') ? 'active' : '' }}" href="{{ route('kyc') }}">KYC</a>
                        <a class="collapse-item {{ Request::is('room_type*') ? 'active' : '' }}" href="{{ route('room_type') }}">Room Type</a>
                        <a class="collapse-item {{ Request::is('floor_master*') ? 'active' : '' }}" href="{{ route('floor_master') }}">Floor Master</a>
                        <a class="collapse-item {{ Request::is('building*') ? 'active' : '' }}" href="{{ route('building') }}">Building Master</a>
                        <a class="collapse-item {{ Request::is('manage_rooms*') ? 'active' : '' }}" href="{{ route('manage_rooms') }}">Manage rooms</a>
                        <a class="collapse-item {{ Request::is('categories') ? 'active' : '' }}" href="{{ route('categories') }}">Category</a>
                        <a class="collapse-item {{ Request::is('sub_category*') ? 'active' : '' }}" href="{{ route('sub_category') }}">Subcategory</a> 
                        <a class="collapse-item {{ Request::is('products*') ? 'active' : '' }}" href="{{ route('products') }}">Products</a>
                        <a class="collapse-item {{ Request::is('payment*') ? 'active' : '' }}" href="{{ route('payment') }}">Payment</a>
                        <a class="collapse-item {{ Request::is('department*') ? 'active' : '' }}" href="{{ route('department') }}">Department</a>
                        <a class="collapse-item {{ Request::is('asset_master*') ? 'active' : '' }}" href="{{ route('asset_master') }}">Asset Master</a>
                        <a class="collapse-item {{ Request::is('categories_master*') ? 'active' : '' }}" href="{{ route('categories_master') }}">Categories Master</a>
                        <a class="collapse-item {{ Request::is('sub_categories_master*') ? 'active' : '' }}" href="{{ route('sub_categories_master') }}">Sub Categories Master</a>
                        <a class="collapse-item {{ Request::is('donation_type_master*') ? 'active' : '' }}" href="{{ route('donation_type_master') }}">Donation Type Master</a>
                        <a class="collapse-item {{ Request::is('payment_mode_master*') ? 'active' : '' }}" href="{{ route('payment_mode_master') }}">Payment Mode master</a>
                        <a class="collapse-item {{ Request::is('bank_account_master*') ? 'active' : '' }}" href="{{ route('bank_account_master') }}">Bank Account Master</a>
                        <a class="collapse-item {{ Request::is('product_with_inventory*') ? 'active' : '' }}" href="{{ route('product_with_inventory') }}">Product with inventory</a>
                        <a class="collapse-item {{ Request::is('product_type_master*') ? 'active' : '' }}" href="{{ route('product_type_master') }}">Product Type master</a>
                        
                        <a class="collapse-item {{ Request::is('unit_master*') ? 'active' : '' }}" href="{{ route('unit_master') }}">Unit Master</a>
                        <a class="collapse-item {{ Request::is('vendor_type_master*') ? 'active' : '' }}" href="{{ route('vendor_type_master') }}">Vendor Type</a>
                        <a class="collapse-item {{ Request::is('supplier_master*') ? 'active' : '' }}" href="{{ route('supplier_master') }}">Supplier Master</a>
                        <a class="collapse-item {{ Request::is('designation_master*') ? 'active' : '' }}" href="{{ route('designation_master') }}">Designation Master</a>
                        <a class="collapse-item {{ Request::is('department_master*') ? 'active' : '' }}" href="{{ route('department_master') }}">Department Master</a>
                        <a class="collapse-item {{ Request::is('books_master*') ? 'active' : '' }}" href="{{ route('books_master') }}">Books Master</a>
                        <a class="collapse-item {{ Request::is('author_master*') ? 'active' : '' }}" href="{{ route('author_master') }}">Author Master</a>
                        <a class="collapse-item {{ Request::is('book_rack_master*') ? 'active' : '' }}" href="{{ route('book_rack_master') }}">Book Rack Master</a>
                        <a class="collapse-item {{ Request::is('donor_Type_master*') ? 'active' : '' }}" href="{{ route('donor_Type_master') }}">Donor Type Master</a>
                        <a class="collapse-item {{ Request::is('donation_purpose_master*') ? 'active' : '' }}" href="{{ route('donation_purpose_master') }}">Donation purpose Master</a>
                        <a class="collapse-item {{ Request::is('location_of_donation*') ? 'active' : '' }}" href="{{ route('location_of_donation') }}">Location of Donation</a>
                        <a class="collapse-item {{ Request::is('donation_activities_master*') ? 'active' : '' }}" href="{{ route('donation_activities_master') }}">Donation Activities Master</a>
                        <a class="collapse-item {{ Request::is('room_status_master*') ? 'active' : '' }}" href="{{ route('room_status_master') }}">Room Status Master</a>
                        <a class="collapse-item {{ Request::is('maintenance_activities_master*') ? 'active' : '' }}" href="{{ route('maintenance_activities_master') }}">Maintenance Activities</a>
                        <a class="collapse-item {{ Request::is('charges_master*') ? 'active' : '' }}" href="{{ route('charges_master') }}">Charges Master</a>
                        <a class="collapse-item {{ Request::is('complaints_types_master*') ? 'active' : '' }}" href="{{ route('complaints_types_master') }}">Complaints types Master</a>
                        <a class="collapse-item {{ Request::is('book_rack_details*') ? 'active' : '' }}" href="{{ route('book_rack_details') }}">Book Rack</a>
                        <a class="collapse-item {{ Request::is('user_master*') ? 'active' : '' }}" href="{{ route('user_master') }}">User Master </a>
                </div>
            </div>
        </li>
    @endcanany
<!-- Masters -->

<!-- Front end room booking -->
    <li class="nav-item {{ Request::is('front_room_booking*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('front_room_booking') }}">
            <i class="fa fa-file-text"></i>
            <span>Front End Room Booking</span>
        </a>
    </li>
<!-- Front end room booking -->

<!-- Transfer room user -->
    <li class="nav-item {{ Request::is('transfer_room_user*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('transfer_room_user') }}">
            <i class="fa fa-file-text"></i>
            <span>Transfer Room User</span>
        </a>
    </li>
<!-- Transfer room user -->

<!-- Standard check-in/out process -->
<li class="nav-item {{ Request::is('check_in_out_process*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('check_in_out_process') }}">
            <i class="fa fa-file-text"></i>
            <span>Check-in/out Process</span>
        </a>
    </li>
<!-- Standard check-in/out process -->

<!-- Maintenance -->
<li class="nav-item {{ Request::is('maintenance*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('maintenance') }}">
            <i class="fa fa-file-text"></i>
            <span>Maintenance</span>
        </a>
    </li>
<!-- Maintenance -->

<!-- Asset allocation to room/building/wings -->
    <li class="nav-item {{ Request::is('asset_allocation_to_room*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('asset_allocation_to_room') }}">
            <i class="fa fa-file-text"></i>
            <span>Asset Allocation to Room</span>
        </a>
    </li>
<!-- Asset allocation to room/building/wings -->

<!-- Donation collection room -->
<li class="nav-item {{ Request::is('donation_collection_room*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('donation_collection_room') }}">
            <i class="fa fa-file-text"></i>
            <span>Donation collection room</span>
        </a>
    </li>
<!-- Donation collection room -->

<!-- Maintenance -->
<li class="nav-item {{ Request::is('staff_allocation*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('staff_allocation') }}">
            <i class="fa fa-file-text"></i>
            <span>Staff Allocation</span>
        </a>
    </li>
<!-- Maintenance --> 

<!-- Room donor details -->
<li class="nav-item {{ Request::is('room_donor_details*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('room_donor_details') }}">
            <i class="fa fa-file-text"></i>
            <span>Room Donor Details</span>
        </a>
    </li>
<!-- Room donor details -->

<!-- Asset Stock Report -->
<li class="nav-item {{ Request::is('asset_stock_report*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('asset_stock_report') }}">
            <i class="fa fa-file-text"></i>
            <span>Asset Stock Report</span>
        </a>
    </li>
<!-- Asset Stock Report -->

<!-- Maintenance Complaints -->
<li class="nav-item {{ Request::is('maintenance_complaints*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('maintenance_complaints') }}">
            <i class="fa fa-file-text"></i>
            <span>Maintenance Complaints</span>
        </a>
    </li>
<!-- Maintenance Complaints -->
   
   
</ul>