<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('dashboard') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">{{ auth()->user()->first_name }}  {{ auth()->user()->last_name }}</div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item {{ Request::is('dashboard*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <hr class="sidebar-divider">
    @canany(['role-create', 'role-edit', 'role-view', 'role-delete', 'permission-create', 'permission-edit', 'permission-view', 'permission-delete', 'access-edit', 'access-view','user-create', 'user-edit', 'user-view', 'user-delete'])
        <!-- <div class="sidebar-heading">
            Access Control
        </div> -->
        <!--li class="nav-item {{ (Request::is('role*') || Request::is('permission*') || Request::is('user*')) ? 'active' : '' }}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                aria-expanded="false" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Access Control</span>
            </a>
            <div id="collapseTwo" class="collapse {{ (Request::is('role*') || Request::is('permission*') || Request::is('user*')) ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    @canany(['role-create', 'role-edit', 'role-view', 'role-delete'])
                        <a class="collapse-item {{ Request::is('role*') ? 'active' : '' }}" href="{{ route('role') }}">Roles</a>
                    @endcanany
                    @canany(['permission-create', 'permission-edit', 'permission-view', 'permission-delete'])
                        <a class="collapse-item {{ Request::is('permission*') ? 'active' : '' }}" href="{{ route('permission') }}">Permission</a>
                    @endcanany
                    <!-- @canany(['access-edit', 'access-view'])
                        <a class="collapse-item {{ Request::is('access_permission*') ? 'active' : '' }}" href="{{ route('access_permission') }}">Access</a>
                    @endcanany
                    @canany(['user-create','user-edit','user-view','user-delete'])
                        <a class="collapse-item {{ Request::is('user*') ? 'active' : '' }}" href="{{ route('user') }}">Users</a>
                    @endcanany
                </div>
            </div>
        </li > !-->
    @endcanany
<!-- Access Control -->

<!-- User -->
   <!--  @canany(['user-create','user-edit','user-view','user-delete'])
    <li class="nav-item {{ Request::is('user*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('user') }}">
            <i class="fa fa-users"></i>
            <span>Users</span>
        </a>
    </li>
    @endcanany
 User -->
 

<!-- Masters -->

@canany(['orders-create','orders-edit','clubs','tee','orders-view','orders-delete','transfer-create','transfer-edit','transfer-view','transfer-delete','products-create','products-edit','products-view','products-delete','taxes-create','taxes-edit','taxes-view','taxes-delete','insurance-create','insurance-edit','insurance-view','insurance-delete','extand_warranties-create','extand_warranties-edit','extand_warranties-view','extand_warranties-delete','fasttags-create','fasttags-edit','fasttags-view','fasttags-delete','finance-create','finance-edit','finance-view','finance-delete','branches-create','branches-edit','branches-view','branches-delete','department-create','department-edit','department-view','department-delete','lead-create','lead-edit','lead-view','lead-delete' ,'accessories-create','accessories-edit','accessories-view','accessories-delete'])
        <li class="nav-item {{ (Request::is('products*') || Request::is('tax*') || Request::is('insurance*') || Request::is('brands*') || Request::is('fasttag*') || Request::is('finance.*') || Request::is('branches*') || Request::is('department*') ||  Request::is('lead*') ||  Request::is('accessory*') ||  Request::is('asset_master*') ||  Request::is('categories_master*') || Request::is('Business_sub_categories_master*') || Request::is('donation_type_master*') || Request::is('payment_mode_master*') || Request::is('bank_account_master*') || Request::is('product_with_inventory*') || Request::is('product_type_master*') || Request::is('unit_master*') || Request::is('vendor_type_master*') || Request::is('supplier_master*') || Request::is('designation_master*') || Request::is('department_master*') || Request::is('books_master*') || Request::is('author_master*') || Request::is('book_rack_master*') || Request::is('donor_Type_master*') || Request::is('donation_purpose_master*')||Request::is('location_of_donation*') || Request::is('donation_activities_master*')||Request::is('room_status_master*')|| Request::is('maintenance_activities_master*')||Request::is('charges_master*')|| Request::is('complaints_types_master*')|| Request::is('book_rack_details*')|| Request::is('user_master*')  ) ? 'active' : '' }}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree"
                aria-expanded="false" aria-controls="collapseThree">
                <i class="fas fa-asterisk"></i>
                <span>System Manager</span>
            </a>
            <div id="collapseThree" class="collapse {{ (Request::is('accessory*')) ? 'show' : '' }}" aria-labelledby="headingOne" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                      
                        <a class="collapse-item {{ Request::is('member*') ? 'active' : '' }}" href="{{ route('member') }}">User  Master</a>
                        <a class="collapse-item {{ Request::is('tee*') ? 'active' : '' }}" href="{{ route('tee') }}">Tee List</a>
                        <a class="collapse-item {{ Request::is('course*') ? 'active' : '' }}" href="{{ route('course') }}">Course Master</a> 
                        <a class="collapse-item {{ Request::is('clubs*') ? 'active' : '' }}" href="{{ route('clubs') }}"> Clubs </a>
                        <a class="collapse-item {{ Request::is('plan*') ? 'active' : '' }}"href="{{ route('plan') }}">Plan</a>
                        <a class="collapse-item {{ Request::is('Payment*') ? 'active' : '' }}"href="{{ route('payment') }}">Payment</a>
                        <a class="collapse-item {{ Request::is('round*') ? 'active' : '' }}"href="{{ route('round') }}">Round</a>

                </div>
            </div>
        </li>
    @endcanany

</ul>
