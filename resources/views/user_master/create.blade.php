@extends('template.app')

@section('meta')
@endsection

@section('title')
    User Insert
@endsection

@section('styles')
<link href="{{ asset('assets/css/dropify.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/sweetalert2.bundle.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Users</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('user_master') }}" class="text-muted">Users</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Insert</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('user_master.insert') }}" name="form" id="form" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('POST')
   <input type="hidden" name="id" value="">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Plese enter first name" value="">
                                <span class="kt-form__help error name"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="department" name="department" placeholder="Plese select department">Department</label>
                                <select class="form-control" name="department" id="department">
                                <option value=""> select department</option>
                                   
                                   <option value="user">user</option>  
                                   <option value="admin">admin</option>
                            
								 <!--  <option value="Sales Department">Sales Department</option>-->
                                   
								   
                                </select>
                                <span class="kt-form__help error department"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="designation" name="designation" placeholder="Plese select designation">Designation</label>
                                <select class="form-control" name="designation" id="designation">
                                    <option value="">Select Designation</option>
                                   
                                   
                                   <option value="electrician">electrician</option>  
                                   <option value="doctor">doctor</option>
                                   <option value="CEO">CEO</option>
                                </select>
                                <span class="kt-form__help error designation"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="contact_number">Phone number</label>
                                <input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="Plese enter contact number" value="">
                                <span class="kt-form__help error phone_number"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="email">Email ID</label>
                                <input type="email" name="email" class="form-control" placeholder="Plese enter email" value="">
                                <span class="kt-form__help error email"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="address">Address</label>
                                <input type="text" name="address" class="form-control" placeholder="Plese enter address" value="">
                                <span class="kt-form__help error address"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="country">Country</label>
                                <input type="text" name="country" class="form-control" placeholder="Plese enter country" value="">
                                <span class="kt-form__help error country"></span>
                            </div>
                            
                            <div class="form-group col-sm-6">
                                <label for="state">State</label>
                                <input type="text" name="state" class="form-control" placeholder="Plese enter state" value="">
                                <span class="kt-form__help error state"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="city">City</label>
                                <input type="text" name="city" class="form-control" placeholder="Plese enter city" value="">
                                <span class="kt-form__help error city"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="role" name="role" placeholder="Plese select role">User Roles</label>
                                <select class="form-control" name="role" id="role">
                                    <option value="">Select role</option>
                                   
                                        <option value="electrician">electrician</option>  
                                        <option value="doctor">doctor</option>
                                        <option value="CEO">CEO</option>
                                                                        
                                </select>
                                <span class="kt-form__help error role"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="permission_management">Permission management with user rights</label>
                                <input type="text" name="permission_management" class="form-control" placeholder="Plese enter permission_management" value="">
                                <span class="kt-form__help error permission_management"></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="profile">Profile</label>
                                <input type="file" name="photo" id="photo" class="form-control">
                                <span class="kt-form__help error photo"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary" >Submit</button>
                            <a href="{{ route('user_master') }}" class="btn waves-effect waves-light btn-rounded btn-outline-secondary">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/js/dropify.min.js') }}"></script>
<script src="{{ asset('assets/js/promise.min.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert2.bundle.js') }}"></script>

<script>
    $(document).ready(function(){
        $("#contact_number").keypress(function(e){
            var keyCode = e.keyCode || e.which;
            var $this = $(this);
            //Regex for Valid Characters i.e. Numbers.
            var regex = new RegExp("^[0-9\b]+$");

            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            // for 10 digit number only
            if ($this.val().length > 9) {
                e.preventDefault();
                return false;
            }
            if (e.charCode < 54 && e.charCode > 47) {
                if ($this.val().length == 0) {
                    e.preventDefault();
                    return false;
                } else {
                    return true;
                }
            }
            if (regex.test(str)) {
                return true;
            }
            e.preventDefault();
            return false;
        });
    });
</script>
<script>
    // $(document).ready(function(){
    //     $('.dropify').dropify({
    //         messages: {
    //             'default': 'Drag and drop profile image here or click',
    //             'remove':  'Remove',
    //             'error':   'Ooops, something wrong happended.'
    //         }
    //     });

    //     var drEvent = $('.dropify').dropify();

    //     var dropifyElements = {};
    //     $('.dropify').each(function () {
    //         dropifyElements[this.id] = false;
    //     });

    //     drEvent.on('dropify.beforeClear', function(event, element){
    //         id = event.target.id;
    //         if(!dropifyElements[id]){
    //             var url = "";
    //             <?php if(isset($data) && isset($data->id)){ ?>
    //                 var id_encoded = "{{ base64_encode($data->id) }}";

    //                 Swal.fire({
    //                     title: 'Are you sure want delete this image?',
    //                     text: "",
    //                     type: 'warning',
    //                     showCancelButton: true,
    //                     confirmButtonColor: '#3085d6',
    //                     cancelButtonColor: '#d33',
    //                     confirmButtonText: 'Yes'
    //                 }).then(function (result){
    //                     if (result.value){
    //                         $.ajax({
    //                             url: url,
    //                             type: "POST",
    //                             data:{
    //                                 id: id_encoded,
    //                                 _token: "{{ csrf_token() }}"
    //                             },
    //                             dataType: "JSON",
    //                             success: function (data){
    //                                 if(data.code == 200){
    //                                     Swal.fire('Deleted!', 'Deleted Successfully.', 'success');
    //                                     dropifyElements[id] = true;
    //                                     element.clearElement();
    //                                 }else{
    //                                     Swal.fire('', 'Failed to delete', 'error');
    //                                 }
    //                             },
    //                             error: function (jqXHR, textStatus, errorThrown){
    //                                 Swal.fire('', 'Failed to delete', 'error');
    //                             }
    //                         });
    //                     }
    //                 });

    //                 return false;
    //             <?php } else { ?>
    //                 Swal.fire({
    //                     title: 'Are you sure want delete this image?',
    //                     text: "",
    //                     type: 'warning',
    //                     showCancelButton: true,
    //                     confirmButtonColor: '#3085d6',
    //                     cancelButtonColor: '#d33',
    //                     confirmButtonText: 'Yes'
    //                 }).then(function (result){
    //                     if (result.value){
    //                         Swal.fire('Deleted!', 'Deleted Successfully.', 'success');
    //                         dropifyElements[id] = true;
    //                         element.clearElement();
    //                     }else{
    //                         Swal.fire('Cancelled', 'Discard Last Operation.', 'error');
    //                     }
    //                 });
    //                 return false;
    //             <?php } ?>
    //         } else {
    //             dropifyElements[id] = false;
    //             return true;
    //         }
    //     });
    // });
</script>

<script>
    $(document).ready(function() {
        var form = $('#form');
        $('.kt-form__help').html('');
        form.submit(function(e) {
            $('.help-block').html('');
            $('.m-form__help').html('');
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: new FormData($(this)[0]),
                dataType: 'json',
                async: false,
                processData: false,
                contentType: false,
                success: function(json) {
                    return true;
                },
                error: function(json) {
                    if (json.status === 422) {
                        e.preventDefault();
                        var errors_ = json.responseJSON;
                        $('.kt-form__help').html('');
                        $.each(errors_.errors, function(key, value) {
                            $('.' + key).html(value);
                        });
                    }
                }
            });
        });
    });
</script>
@endsection