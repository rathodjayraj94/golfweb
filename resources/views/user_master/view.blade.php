@extends('template.app')

@section('meta')
@endsection

@section('title')
    User View
@endsection

@section('styles')
<link href="{{ asset('assets/css/dropify.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Users</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('user_master') }}" class="text-muted">Users</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">View</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="" name="form" id="form" method="post" enctype="multipart/form-data">
                        <div class="row">
						  <input type="hidden" name="id" value="{{ $data->id }}">
                            <div class="form-group col-sm-6">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Plese enter first name" value="{{ $data->name}}" readonly>
                                <span class="kt-form__help error name"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="department" name="department" placeholder="Plese select department">Department</label>
                               <!-- <select class="form-control" name="department" id="department" readonly>
                                    <option value="">Select Department</option>
                                    <option value="Sales Department" selected>Sales Department</option>
                                    
                                </select>-->
								  <input type="text" name="department" class="form-control" placeholder="" value="{{ $data->department }}" readonly>
                                <span class="kt-form__help error department"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="designation" name="designation" placeholder="Plese select designation">Designation</label>
                                <!--<select class="form-control" name="designation" id="designation" readonly>
                                    <option value="">Select Designation</option>
                                    <option value="test" selected>Test</option>
                                </select>-->
                     			  <input type="text" name="designation" class="form-control" placeholder="" value="{{ $data->designation }}" readonly>
								<span class="kt-form__help error designation"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="contact_number">Phone number</label>
                                <input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="Plese enter contact number" value="{{ $data->phone_number}}" readonly>
                                <span class="kt-form__help error phone_number"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="email">Email ID</label>
                                <input type="email" name="email" class="form-control" placeholder="Plese enter email" value="{{ $data->email }}" readonly>
                                <span class="kt-form__help error email"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="address">Address</label>
                                <input type="text" name="address" class="form-control" placeholder="Plese enter address" value="{{ $data->address }}" readonly>
                                <span class="kt-form__help error address"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="country">Country</label>
                                <input type="text" name="country" class="form-control" placeholder="Plese enter country" value="{{ $data->country }}" readonly>
                                <span class="kt-form__help error country"></span>
                            </div>
                            
                            <div class="form-group col-sm-6">
                                <label for="state">State</label>
                                <input type="text" name="state" class="form-control" placeholder="Plese enter state" value="{{ $data->state }}" readonly>
                                <span class="kt-form__help error state"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="city">City</label>
                                <input type="text" name="city" class="form-control" placeholder="Plese enter city" value="{{ $data->city }}" readonly>
                                <span class="kt-form__help error city"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="role"  >User Roles</label>
                                <input type="text" name="role" class="form-control" placeholder="" value="{{ $data->role}}" readonly>
							 <!-- <select class="form-control" name="role" id="role" readonly>
                                    <option value="">Select role</option>
                                    <option value="Admin" selected>Admin</option>
                                   
                                </select>-->
                                <span class="kt-form__help error role"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="permission_management">Permission management with user rights</label>
                                <input type="text" name="permission_management" class="form-control" placeholder="Plese enter permission_management" value="{{ $data->permission_management }}" readonly>
                                <span class="kt-form__help error permission_management"></span>
                            </div>
                         <!-- <div class="form-group col-sm-12">
                                <label for="profile">Profile</label>
                                <img src="{{$data->photo}}"></img>
								<input type="image" name="photo" id="photo" class="form-control" src="{{ $data->photo }}" readonly accept="image/*">
                                <span class="kt-form__help error photo"></span>
                            </div>-->
                        </div>
                        <div class="form-group">
                            <a href="{{ route('user_master') }}" class="btn waves-effect waves-light btn-rounded btn-outline-secondary">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/js/dropify.min.js') }}"></script>

<script>
    // $(document).ready(function(){
    //     var drEvent = $('.dropify').dropify();
    // });
</script>
@endsection

