@extends('template.app')

@section('meta')
@endsection

@section('title')
service Update
@endsection

@section('styles')
<link href="{{ asset('assets/css/dropify.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/sweetalert2.bundle.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Course Master</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('course') }}" class="text-muted">Course Master</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Update</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('course.update') }}" name="form" id="form" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <input type="hidden" name="id" value="{{$data->id}}">
                        <div class="row">
                            
                            <div class="form-group col-sm-4">
                                <label for="service_type">Zip Code</label>
                                <input class="form-control" type="text" name="zip_code" id="zip_code" value="{{$data->zip_code}}" placeholder="Zip Code ">
                                <span class="kt-form__help error service_type"></span>
                            </div>
                           
                            <!--div class="form-group col-sm-4">
                            <label for="name">Hole File </label>
                                <input class="form-control" type="file" name="holefile" id="file">
                                <span class="kt-form__help error file"></span>
                            </div>
                            <div class="form-group col-sm-3">
                                    <?php if($data->file ) { ?>
                                <a href="<?php  echo url('').'/public/images/'.$data->file; ?>" download>
                                        <?php echo $data->file;?> <?php } ?> </a>
                                <input type="hidden" name="oldhole" id="oldhole"  value="{{ $data->holefile ??'' }}">

                            </div>

                            <div class="form-group col-sm-4">
                                <label for="name">Ladmark File </label>
                                <input class="form-control" type="file" name="file" id="file">
                                <span class="kt-form__help error file"></span>
                            </div>
                            <div class="form-group col-sm-3">
                            <label for="name"> </label>
                                    <?php if($data->file ) { ?>
                                <a href="<?php  echo url('').'/public/images/'.$data->file; ?>" download>
                                        <?php echo $data->file;?> <?php } ?> </a>
                                <input type="hidden" name="oldlogo" id="oldlogo"  value="{{ $data->file ??'' }}">

                            </div !-->


                          
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary" >Submit</button>
                            <a href="{{ route('course') }}" class="btn waves-effect waves-light btn-rounded btn-outline-secondary">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/js/promise.min.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert2.bundle.js') }}"></script>

<script>
    $(document).ready(function() {
        var form = $('#form');
        $('.kt-form__help').html('');
        form.submit(function(e) {
            $('.help-block').html('');
            $('.m-form__help').html('');
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: new FormData($(this)[0]),
                dataType: 'json',
                async: false,
                processData: false,
                contentType: false,
                success: function(json) {
                    return true;
                },
                error: function(json) {
                    if (json.status === 422) {
                        e.preventDefault();
                        var errors_ = json.responseJSON;
                        $('.kt-form__help').html('');
                        $.each(errors_.errors, function(key, value) {
                            $('.' + key).html(value);
                        });
                    }
                }
            });
        });
    });
</script>
@endsection