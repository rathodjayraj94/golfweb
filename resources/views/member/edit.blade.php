@extends('template.app')

@section('meta')
@endsection

@section('title')
member Update
@endsection

@section('styles')
<link href="{{ asset('assets/css/dropify.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/sweetalert2.bundle.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">USER</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('member') }}" class="text-muted">member</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Update</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('member.update') }}" name="form" id="form" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <input type="hidden" name="id" value="{{$data->id}}">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="image">Image</label>
                                <input class="form-control" type="file" name="image" id="image" placeholder="image ">
                                <span class="kt-form__help error image"></span>
                            </div>
                            <div class="form-group col-sm-2">
                                    <?php if($data->image ) { ?>
                                <img src="<?php  echo url('').'/public/images/'.$data->image; ?>" height="50px;">
                                        <?php } ?>
                            </div>
                            <input type="hidden" name="oldlogo" id="oldlogo"  value="{{ $data->image ??'' }}">

                            <div class="form-group col-sm-6">
                                <label for="full_name">Full Name</label>
                                <input class="form-control" type="text" name="full_name" id="full_name" placeholder="Full Name " value="{{ $data->full_name ??'' }}">
                                <span class="kt-form__help error full_name"></span>
                            </div>
                            

                            <div class="form-group col-sm-6">
                                <label for="whatsapp_no">Mobile  no</label>
                                <input class="form-control" type="text" name="whatsapp_no" id="whatsapp_no" placeholder="Whatsapp no " value="{{ $data->whatsapp_no ??'' }}">
                                <span class="kt-form__help error Whatsapp no"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="email_id">Email-id</label>
                                <input class="form-control" type="text" name="email_id" id="email_id" placeholder="Email-id" value="{{ $data->email_id ??'' }}">
                                <span class="kt-form__help error email_id"></span>
                            </div>
                          

                            <div class="form-group mt-3 ml-3">
                                <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary" >Submit</button>
                                <a href="{{ route('member') }}" class="btn waves-effect waves-light btn-rounded btn-outline-secondary">Back</a>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/js/promise.min.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert2.bundle.js') }}"></script>

<script>
    $(document).ready(function() {
        var form = $('#form');
        $('.kt-form__help').html('');
        form.submit(function(e) {
            $('.help-block').html('');
            $('.m-form__help').html('');
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: new FormData($(this)[0]),
                dataType: 'json',
                async: false,
                processData: false,
                contentType: false,
                success: function(json) {
                    return true;
                },
                error: function(json) {
                    if (json.status === 422) {
                        e.preventDefault();
                        var errors_ = json.responseJSON;
                        $('.kt-form__help').html('');
                        $.each(errors_.errors, function(key, value) {
                            $('.' + key).html(value);
                        });
                    }
                }
            });
        });
    });
</script>
@endsection