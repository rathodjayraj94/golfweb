<?php

namespace App\Imports;

use App\Models\Fasttag;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class ImportFasttag implements ToModel,WithHeadingRow 
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $data = [
            'tag_id' => $row['tag_id'],
            'amount' => $row['amount'],
            'status' =>$row['status'],
        ];
        $fasttag = Fasttag::create($data);
        return $fasttag;
    }
}
