<?php

namespace App\Imports;

use App\Models\ExtandWarranty;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class ImportExtandWarranty implements ToModel,WithHeadingRow 
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $data = [
            'years' => $row['years'],
            'amount' => $row['amount'],
            'status' =>$row['status'],
        ];
        $extandWarranty = ExtandWarranty::create($data);
        return $extandWarranty;
    }
}
