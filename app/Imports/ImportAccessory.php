<?php

namespace App\Imports;

use App\Models\Accessory;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class ImportAccessory implements ToModel,WithHeadingRow 
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $data = [
            'name' => $row['name'],
            'type' =>$row['type'],
            'price' =>$row['price'],
            'hsn_number' =>$row['hsn_number'],
            'model_number' =>$row['model_number'],
            'model_type' =>$row['model_type'],
            'warranty' =>$row['warranty'],
            'status' =>$row['status'],
        ];
        $Accessory = Accessory::create($data);
        return $Accessory;
    }
}
