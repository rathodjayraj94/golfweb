<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class ImportUser implements ToModel,WithHeadingRow 
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    // public function startRow(): int
    // {
        
    //     return 0;
    // }
    public function model(array $row)
    {
      
        $data = [
            'first_name' => $row['first_name'],
            'last_name' => $row['last_name'],
            'contact_number' =>$row['contact_number'],
            'email' => $row['email'],
            'password' => bcrypt($row['password']),
            'status' => 'active',
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => 1,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => 1
        ];
        $user = User::create($data);
       // $user->assignRole($row[5]);
        return $user;
    }
}
