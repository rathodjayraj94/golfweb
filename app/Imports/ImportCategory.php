<?php

namespace App\Imports;

use App\Models\Department;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\BusinessCategoriesMaster;

class ImportCategory implements ToModel,WithHeadingRow 
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
       $data = [
            'name' => $row['name'],
            'status' => 'active',
            
        ]; 


        $category = BusinessCategoriesMaster::create($data);
        return $category;
    }
}
