<?php

namespace App\Imports;

use App\Models\Branch;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class ImportBranch implements ToModel,WithHeadingRow 
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    // public function startRow(): int
    // {
        
    //     return 0;
    // }
    public function model(array $row)
    {
      
        $data = [
            'name' => $row['name'],
            'city' => $row['city'],
            'address' =>$row['address'],
            'email' => $row['email'],
            'contact_number' => $row['contact_number'],
            'manager' => $row['manager'],
            'manager_contact_number' => $row['manager_contact_number'],
            'status'=>'active',
            'created_by' => 1,
            'updated_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
            
        ];
       $branch = Branch::create($data);
       // $user->assignRole($row[5]);
        return $branch;
    }
}
