<?php

namespace App\Imports;

use App\Models\Department;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\region;

class ImportDepartment implements ToModel,WithHeadingRow 
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
       $data = [
            'name' => $row['name'],
            'branch_id' => $row['branch_id'],
            'email' => $row['email'],
            'number' => $row['number'],
            'authorised_person' => $row['authorised_person'],
            'status' =>$row['status'],
        ]; 


        $department = Department::create($data);
        return $department;
    }
}
