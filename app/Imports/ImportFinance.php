<?php

namespace App\Imports;

use App\Models\Finance;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class ImportFinance implements ToModel,WithHeadingRow 
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $data = [
            'name' => $row['name'],
            'branch_id' => $row['branch_id'],
            'dsa_or_broker' => $row['dsa_or_broker'],
            'status' =>$row['status'],
        ];
        $Finance = Finance::create($data);
        return $Finance;
    }
}
