<?php

namespace App\Imports;

use App\Models\Category;
use App\Models\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class ImportProduct implements ToModel ,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function startRow(): int
    {
        return 7;
    }
    public function model(array $row)
    {
       
        // $category = (object)'';
        // if(isset($row['category_id']) && !empty($row['name'])){
        //     $category = Category::select('id')->where(['name' => $row['category_id']])->first();
        //     if(!$category){
        //         session(['error' => 'No Product Category Found In at least one record!']);
        //         return null;
        //     }
        // }else{
        //     $category->id = null;
        // }
        $data = [
            'category_id' => $row['category_id'],
            'name' => $row['name'],
            'veriant' =>$row['veriant'],
            'ex_showroom_price' => $row['ex_showroom_price'],
            'interior_color' => $row['interior_color'],
            'exterior_color' => $row['exterior_color'],
            'is_applicable_for_mcp' => $row['is_applicable_for_mcp'],
            'status' => 'active',
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => auth()->user()->id,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->user()->id
        ];
        
        $products = Product::create($data);
        return $products;
    }
}
