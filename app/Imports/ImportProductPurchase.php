<?php

namespace App\Imports;

use App\Models\ProductPurchase;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Department;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class ImportProductPurchase implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ProductPurchase([
            //

            
           
                // 'id' => $row['id'],
                // 'date' => $row['date'],
                // 'department_name' =>$row['department_name'],
                // 'order_by' => $row['order_by'],
                // 'sr_no' => $row['sr_no'],
                // 'product_name' => $row['product_name'],
                // 'unit_type' => $row['unit_type'],
                // 'qty' => $row['qty'],
                // 'requirement_date' => $row['requirement_date'],
                // 'vendor' => $row['vendor'],
                // 'purchased_ref' => $row['purchased_ref'],
                // 'purchased_purpose' => $row['purchased_purpose'],
                // 'remarks'=> $row['remarks']     
                
                
                'date' => $row[0],
                'department_name' =>$row[1],
                'order_by' => $row[2],
                'sr_no' => $row[3],
                'product_name' => $row[4],
                'unit_type' => $row[5],
                'qty' => $row[6],
                'requirement_date' => $row[7],
                'vendor' => $row[8],
                'purchased_ref' => $row[9],
                'purchased_purpose' => $row[10],
                'remarks'=> $row[11]     
               
           
    
        ]);
        $products = ProductPurchase::create($data);
        return $products ;
    }


}
