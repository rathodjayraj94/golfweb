<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ClubMaster extends Model{
    use HasFactory;
    protected $table = 'club_master';
    
    protected $fillable = [
  
        'name',
        'status',
        'created_at'
    ];

    
}
