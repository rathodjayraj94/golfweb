<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;



class course extends Model
{
    
        use HasFactory;
        protected $table = 'course';
        
        protected $fillable = [
            'course_name',
            'zip_code',
            'lat',
            'long',
            'file',
            'total_hole',
            'status',
            'created_at',
            'updated_at'
        ];
}
