<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class match_round extends Model
{
    use HasFactory;
    protected $table = 'round';
    
    protected $fillable = [
        'course_id',
		'status',
        'created_at'
    ];
}