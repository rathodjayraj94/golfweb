<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class TeeMaster extends Model{
    use HasFactory;
    protected $table = 'tee_master';
    
    protected $fillable = [
        'name',
        'status',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
        'categories_id'
    ];

    
}
