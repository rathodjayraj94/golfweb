<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;

class UsersMaster extends Model{
    use HasFactory;

    protected $table = 'user_master';

    public function guardName(){
        return "web";
    }

    protected $fillable = [
                'name',
                'department',
                'designation',
                'phone_number',
                'email',
                'address',
                'country',
                'state',
                'city',
                'permission_management',
                'role',
                'photo',
                'status',
                'created_at',
                'created_by',
                'updated_at',
                'updated_by'
    ];
}
