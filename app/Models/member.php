<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class member extends Model
{
    use HasFactory;
    protected $table = 'member';
    
    protected $fillable = [
    'status' => 'active',
            'image',
            'full_name',
            'whatsapp_no',
            'email_id',
            'password',
            'created_at',
            'updated_at' 
    ];

}