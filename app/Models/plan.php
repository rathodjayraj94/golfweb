<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class plan extends Model
{
    use HasFactory;
    protected $table = 'plan';
    
    protected $fillable = [
        'plan_name',
        'plan_amount',
         'days',
        'plan_type',
        'status',
        'created_at',
        'updated_at'
    ];
}