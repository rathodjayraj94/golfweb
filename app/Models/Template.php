<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    use HasFactory;

    protected $table = 'template';
    
    protected $fillable = [
        'email_data',
        'thank_you_letter',
        'status',        
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    ];
}
