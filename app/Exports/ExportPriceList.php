<?php

namespace App\Exports;

use App\Models\pricelist;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExportPriceList implements FromCollection, WithHeadings,WithStyles ,ShouldAutoSize{
    function __construct($slug) {
        $this->slug = $slug;
    }

    public function collection(){
        $model = new Pricelist();
        $data = $model->export($this->slug);

        if($data == null){
            return throw new \ErrorException('No data found');
        }else{
            return $data;
        }
    }

    public function headings() :array{
        return ['car_model', 'colour_type', 'transmission', 'ex_showroom_price', 'tcs', 'statutory_charges', 'registration_individual_charges', 'registration_corporate_charges', 'insurance', 'fasttag', 'on_road_individual_charges', 'on_road_corporate_charges', 'extended_warranty', 'accessories_kit', 'total_road_individual_charges', 'total_road_corporate_charges'];
    }

    public function styles(Worksheet $sheet){
        return [1 => ['font' => ['bold' => true]]];
    }
}