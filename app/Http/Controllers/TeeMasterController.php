<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Exports\ExportDepartment;
use Maatwebsite\Excel\Facades\Excel;
use Auth, DB, Mail, Validator, File, DataTables, Exception;
use App\Imports\ImportDepartment;
use App\Models\TeeMaster;

class TeeMasterController extends Controller{
   


    /** index */
        public function index(Request $request){
            if($request->ajax()){
                
                $data = TeeMaster::select('*')->orderBy('id','desc')->get();                
                    return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($data)
                        {
                            $return = '<div class="btn-group">';

                        
                            $return .= '<a href="'.route('tee.edit' , ['id' => base64_encode($data->id)]).'" class="btn btn-default btn-xs">
                                            <i class="fa fa-pencil"></i>
                                        </a> &nbsp;';
                    

                            $return .= '<a href="javascript:;" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-bars"></i>
                                            </a> &nbsp;
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="active" data-id="' . base64_encode($data->id) . '">Active</a></li>
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="inactive" data-id="' . base64_encode($data->id) . '">Inactive</a></li>
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="deleted" data-id="' . base64_encode($data->id) . '">Delete</a></li>
                                            </ul>';
                        

                            $return .= '</div>';

                            return $return;
                        })

                    
                        ->editColumn('status', function ($data)
                         {
                            if ($data->status == 'active') {
                                return '<span class="badge badge-pill badge-success">Active</span>';
                            } else if ($data->status == 'inactive') {
                                return '<span class="badge badge-pill badge-warning">Inactive</span>';
                            } else if ($data->status == 'deleted') {
                                return '<span class="badge badge-pill badge-danger">Deleted</span>';
                            }else{
                                return '-';
                            }
                        })

                        ->rawColumns([ 'action' ,'status'])
                        ->make(true);
                }
            return view('tee.index');
        }

        public function create(Request $request)
        {
            
            return view('tee.create');
        }
    /** create */

    /** insert */
        public function insert(Request $request){
            if($request->ajax()){
                return true ;
            }
            $crud = [
                'name' => $request->name,
                'status' => 'active',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => auth()->user()->id,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => auth()->user()->id,
                'categories_id'=> $request->categories_id,
            ];
            
            DB::beginTransaction();
            try {
                DB::enableQueryLog();
                $last_id = TeeMaster::insertGetId($crud);
                if ($last_id) {
                    
                    DB::commit();
                    return redirect()->route('tee')->with('success', 'Record inserted successfully');
                } else {
                    DB::rollback();
                    return redirect()->back()->with('error', 'Failed to insert record')->withInput();
                }
            } catch (\Throwable $th) {
                DB::rollback();
                return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
            }
        }
    /** insert */

    /** view */
        public function view(Request $request){  
           
            return view('tee.view');
        }
    /** view */

    /** edit */
        public function edit(Request $request){
            $id = base64_decode($request->id);
            $data = TeeMaster::where(['id' => $id])->first();
            return view('tee.edit')->with(['data' => $data]);
        }
    /** edit */

    /** update */
        public function update(Request $request){
            if($request->ajax()){
                return true ;
            }
            // $branch_admin=$request->branch_admin ? 1 : NULL;
            $crud = [
                'name' => $request->name,
                'status' => 'active',
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => auth()->user()->id
            ];
           
            
            DB::beginTransaction();
            try {
                DB::enableQueryLog();
                $update = TeeMaster::where(['id' => $request->id])->update($crud);
                if($update){
                    DB::commit();
                    return redirect()->route('tee')->with('success', 'Record updated successfully');
                } else {
                    DB::rollback();
                    return redirect()->back()->with('error', 'Failed to update record')->withInput();
                }
            } catch (\Throwable $th) {
                DB::rollback();
                return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
            }
        }
    /** update */

    /** change-status */
        public function change_status(Request $request){
            if (!$request->ajax()) { exit('No direct script access allowed'); }
                
                $id = base64_decode($request->id);
                $data = TeeMaster::where(['id' => $id])->first();
                
                if(!empty($data)){
                    $update = TeeMaster::where(['id' => $id])->update(['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s'), 'updated_by' => auth('sanctum')->user()->id]);
                    if($update){
                        return response()->json(['code' => 200]);
                    }else{
                        return response()->json(['code' => 201]);
                    }
                }else{
                    return response()->json(['code' => 201]);
                }
        }
    /** change-status */

    /** Export */
       public function export(Request $request){
            if(isset($request->slug) && $request->slug != null){
                $slug = $request->slug;
            }else{
                $slug = 'all';
            }

            $name = 'all_sub_categories_master_'.Date('YmdHis').'.xlsx';

            try {
                return Excel::download(new ExportDepartment($slug), $name);
            }catch(\Exception $e){
                return redirect()->back()->with('error', $e->getMessage());
            }
        }
    /** Export */
    /** Import */
    public function import(Request $request){
           
        if(Excel::import(new ImportDepartment, $request->file('file')->store('file'))){
            return redirect()->route('tee')->with('sucess' ,'File Imported Sucessfully');
        }else{
            return redirect()->route('tee')->with('error' ,'Faild To Import File!');
        }
    }
    /** Import */
}
