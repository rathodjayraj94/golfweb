<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Branch;
use App\Models\Department;
use App\Models\plan;
use App\Exports\ExportDepartment;
use Maatwebsite\Excel\Facades\Excel;
use Auth, DB, Mail, Validator, File, DataTables, Exception;
use App\Imports\ImportRegion;

class planController extends Controller
{
    public function __construct(){
        /*$this->middleware('permission:city-create', ['only' => ['create']]);
        $this->middleware('permission:city-edit', ['only' => ['edit']]);
        $this->middleware('permission:city-view', ['only' => ['view']]);
        $this->middleware('permission:city-delete', ['only' => ['delete']]);*/
    }
//  construct

//  index 
    public function index(Request $request){
        if($request->ajax()){
            $data = plan::select('*')->orderBy('id' , 'desc')->get();
            
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($data){
                        $return = '<div class="btn-group">';

                     /*   if(auth()->user()->can('city-edit')){*/
                            $return .= '<a href="'.route('plan.edit',['id' => base64_encode($data->id)]).'" class="btn btn-default btn-xs">
                                            <i class="fa fa-pencil"></i>
                                        </a> &nbsp;';
                     //   }   

                       //if (auth()->user()->can('city-delete')) {
                            $return .= '<a href="javascript:;" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-bars"></i>
                                            </a> &nbsp;
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="active" data-id="' . base64_encode($data->id) . '">Active</a></li>
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="inactive" data-id="' . base64_encode($data->id) . '">Inactive</a></li>
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="deleted" data-id="' . base64_encode($data->id) . '">Delete</a></li>
                                            </ul>';
                       // }

                        $return .= '</div>';

                        return $return;
                    })

                
                    ->editColumn('status', function ($data) {
                        if ($data->status == 'active') {
                            return '<span class="badge badge-pill badge-success">Active</span>';
                        } else if ($data->status == 'inactive') {
                            return '<span class="badge badge-pill badge-warning">Inactive</span>';
                        } else if ($data->status == 'deleted') {
                            return '<span class="badge badge-pill badge-danger">Deleted</span>';
                        }else{
                            return '-';
                        }
                    })

                    ->rawColumns([ 'action' ,'status'])
                    ->make(true);
            }
        return view('plan.index');
    }
//  index 

// create /
    public function create(Request $request){
        
        $data = plan::select('*')->where(['status' => 'active'])->get(); 
        $country = plan::select('*')->where(['status'=>'active'])->get();
        return view('plan.create')->with(['data' => $data,'country'=>$country]);
    }
//  create /
//  insert 
    public function insert(Request $request){
        if($request->ajax()){
            return true ;
        }
        $crud = [
   
            'plan_name' => $request->plan_name,
            'plan_amount' => $request->plan_amount,
            'days' => $request->days,
            'plan_type' =>  $request->plan_type,
            'status' => 'active',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),

        ];
        
        DB::beginTransaction();
        try {
            DB::enableQueryLog();
            $last_id = plan::insertGetId($crud);
            
            if ($last_id) {
                
                DB::commit();
                return redirect()->route('plan')->with('success', 'Record inserted successfully');
            } else {
                DB::rollback();
                return redirect()->back()->with('error', 'Failed to insert record')->withInput();
            }
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
        }
    }
//  insert /

//  view /
    public function view(Request $request){  
        $id = base64_decode($request->id);
                                
        $data = plan::select('*')->where(['id' => $id])->first();
        $country = plan::select('*')->where(['status'=>'active'])->get(); 
        return view('plan.view')->with(['data' => $data,'country'=>$country]);
    }
// view /

// edit /
    public function edit(Request $request){
        $id = base64_decode($request->id);
                                
        $data = plan::select('*')->where(['id' => $id])->first();
        $country = plan::select('*')->where(['status'=>'active'])->get(); 
   
        return view('plan.edit')->with(['data' => $data,'country'=>$country]);;
    }
// edit /

//  update 
    public function update(Request $request){
        if($request->ajax()){
            return true ;
        }
       // $branch_admin=$request->branch_admin ? 1 : NULL;
       $crud = [
   
        'plan_name' => $request->plan_name,
        'plan_amount' => $request->plan_amount,
        'days' => $request->days,
        'plan_type' =>  $request->plan_type,
        'status' => 'active',
        'updated_at' => date('Y-m-d H:i:s'),

    ];
       
        
        DB::beginTransaction();
        try {
            DB::enableQueryLog();
            $update = plan::where(['id' => $request->id])->update($crud);
            if($update){
                DB::commit();
                return redirect()->route('plan')->with('success', 'Record updated successfully');
            } else {
                DB::rollback();
                return redirect()->back()->with('error', 'Failed to update record')->withInput();
            }
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
        }
    }
//  update /

//  change-status /
    public function change_status(Request $request){
        if (!$request->ajax()) { exit('No direct script access allowed'); }
            
            $id = base64_decode($request->id);
            $data = plan::where(['id' => $id])->first();
       
            if(!empty($data)){
                $update = plan::where(['id' => $id])->update(['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')]);
                if($update){
                    return response()->json(['code' => 200]);
                }else{
                    return response()->json(['code' => 201]);
                }
            }else{
                return response()->json(['code' => 201]);
            }
    }
/* change-status /


    /* Import */
    public function import(Request $request){
        
        if(Excel::import(new ImportRegion, $request->file('file')->store('file'))){
            return redirect()->route('region')->with('sucess' ,'File Imported Sucessfully');
        }else{
            return redirect()->route('region')->with('error' ,'Faild To Import File!');
        }
    }

}
