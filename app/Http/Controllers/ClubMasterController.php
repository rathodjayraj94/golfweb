<?php

namespace App\Http\Controllers;

use App\Imports\ImportCategory;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\ClubMaster;
use App\Exports\ExportDepartment;
use Maatwebsite\Excel\Facades\Excel;
use Auth, DB, Mail, Validator, File, DataTables, Exception;

class ClubMasterController extends Controller{
    /** construct */
        public function __construct(){
       
        }
    /** construct */

    /** index */
        public function index(Request $request){
            if($request->ajax()){
                $data = ClubMaster::select('*')->orderBy('id' , 'desc')->get();
                
                    return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($data){
                            $return = '<div class="btn-group">';
                            
                            $return .= '<a href="'.route('clubs.edit' , ['id' => base64_encode($data->id)]).'" class="btn btn-default btn-xs">
                                                <i class="fa fa-pencil"></i>
                                            </a> &nbsp;';
                            $return .= '<a href="javascript:;" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-bars"></i>
                                                </a> &nbsp;
                                                <ul class="dropdown-menu">
                                                    <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="active" data-id="' . base64_encode($data->id) . '">Active</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="inactive" data-id="' . base64_encode($data->id) . '">Inactive</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="deleted" data-id="' . base64_encode($data->id) . '">Delete</a></li>
                                                </ul>';
                           
                            $return .= '</div>';

                            return $return;
                        })

                      
                        ->editColumn('status', function ($data) {
                            if ($data->status == 'active') {
                                return '<span class="badge badge-pill badge-success">Active</span>';
                            } else if ($data->status == 'inactive') {
                                return '<span class="badge badge-pill badge-warning">Inactive</span>';
                            } else if ($data->status == 'deleted') {
                                return '<span class="badge badge-pill badge-danger">Deleted</span>';
                            }else{
                                return '-';
                            }
                        })

                        ->rawColumns([ 'action' ,'status'])
                        ->make(true);
                }
            return view('clubs.index');
        }
  
        public function create(Request $request){
            
            return view('clubs.create');
        }
    /** create */

    /** insert */
        public function insert(Request $request){
          
            if($request->ajax()){
                return true ;
            }

                
            if($request->file('file'))
            {
                $file = $request->file('file');
                $filename = time() . '.' . $request->file('file')->extension();
                $filePath = public_path() . '/images/category/';
                $file->move($filePath, $filename);
              
            }
            else
            {
                $filename = '';
            }
            $crud = [
          
                'name' => $request->name,
                'file' => $filename,
                'status' => 'active',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => auth()->user()->id,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => auth()->user()->id

            ];
            DB::beginTransaction();
            try {
                DB::enableQueryLog();
                $last_id = ClubMaster::insertGetId($crud);
                if ($last_id) {
                    
                    DB::commit();
                    return redirect()->route('clubs')->with('success', 'Record inserted successfully');
                } else {
                    DB::rollback();
                    return redirect()->back()->with('error', 'Failed to insert record')->withInput();
                }
            } catch (\Throwable $th) {
                DB::rollback();
                return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
            }
        }
    /** insert */

    /** view */
        public function view(Request $request){  
            $id = base64_decode($request->id);
            $data = ClubMaster::where(['id' => $id])->first();
            return view('clubs.view')->with(['data' => $data]);;
        }
    /** view */

    /** edit */
        public function edit(Request $request){
            $id = base64_decode($request->id);
            $data = ClubMaster::where(['id' => $id])->first();
            return view('clubs.edit')->with(['data' => $data]);
        }
    /** edit */

    /** update */
        public function update(Request $request){
            if($request->ajax()){
                return true ;
            }
            if($request->file('file'))
            {
                $file = $request->file('file');
                $filename = time() . '.' . $request->file('file')->extension();
                $filePath =  public_path() . '/images/category/';
                $file->move($filePath, $filename);
                
            }
            else
            {
                $filename = $request->oldlogo;
            }
         
          
            $crud = [
               'id' => $request->id,
                'name' => $request->name,
                'file' => $filename,
                'status' => 'active',
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => auth()->user()->id
            ];
           
            
            DB::beginTransaction();
            try {
                DB::enableQueryLog();
                $update = ClubMaster::where(['id' => $request->id])->update($crud);
                if($update){
                    DB::commit();
                    return redirect()->route('clubs')->with('success', 'Record updated successfully');
                } else {
                    DB::rollback();
                    return redirect()->back()->with('error', 'Failed to update record')->withInput();
                }
            } catch (\Throwable $th) {
                DB::rollback();
                return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
            }
        }
    /** update */

    /** change-status */
        public function change_status(Request $request)
        {
            if (!$request->ajax()) { exit('No direct script access allowed'); }
                
                $id = base64_decode($request->id);
                $data = ClubMaster::where(['id' => $id])->first();
                
                if(!empty($data)){
                    $update = ClubMaster::where(['id' => $id])->update(['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s'), 'updated_by' => auth('sanctum')->user()->id]);
                    if($update){
                        return response()->json(['code' => 200]);
                    }else{
                        return response()->json(['code' => 201]);
                    }
                }else{
                    return response()->json(['code' => 201]);
                }
        }
  
    public function import(Request $request){
         
        if(Excel::import(new ImportCategory, $request->file('file')->store('file'))){
            return redirect()->route('clubs')->with('sucess' ,'File Imported Sucessfully');
        }else{
            return redirect()->route('clubs')->with('error' ,'Faild To Import File!');
        }
    }
    /** Import */
}
