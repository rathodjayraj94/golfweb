<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\match_round;
use App\Exports\ExportDepartment;
use Maatwebsite\Excel\Facades\Excel;
use Auth, DB, Mail, Validator, File, DataTables, Exception;
use App\Imports\ImportRegion;

class RoundController extends Controller
{
  

//  index 
    public function index(Request $request){
        if($request->ajax()){
            $data = DB::table('round')
            ->select('round.*','c.course_name as course_name','c.zip_code as zip_code')
            ->leftjoin('course as c', 'c.id', 'round.course_id')
            ->orderBy('round.id' , 'desc')->get();
            
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($data){
                        $return = '<div class="btn-group">';

                       /* if(auth()->user()->can('city-view')){*/
                           $return .= '<a href="'.route('round.view',['id' => base64_encode($data->id)]).'" class="btn btn-default btn-xs">
                                            <i class="fa fa-eye"></i>
                                        </a> &nbsp;';
                   
                       //if (auth()->user()->can('city-delete')) {
                            $return .= '<a href="javascript:;" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-bars"></i>
                                            </a> &nbsp;
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="active" data-id="' . base64_encode($data->id) . '">Active</a></li>
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="inactive" data-id="' . base64_encode($data->id) . '">Inactive</a></li>
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="deleted" data-id="' . base64_encode($data->id) . '">Delete</a></li>
                                            </ul>';
                       // }

                        $return .= '</div>';

                        return $return;
                    })

                
                    ->editColumn('status', function ($data) {
                        if ($data->status == 'active') {
                            return '<span class="badge badge-pill badge-success">Active</span>';
                        } else if ($data->status == 'inactive') {
                            return '<span class="badge badge-pill badge-warning">Inactive</span>';
                        } else if ($data->status == 'deleted') {
                            return '<span class="badge badge-pill badge-danger">Deleted</span>';
                        }else{
                            return '-';
                        }
                    })

                    ->rawColumns([ 'action' ,'status'])
                    ->make(true);
            }
        return view('round.index');
    }

    public function view(Request $request){  
        $id = base64_decode($request->id);
                                
		 $data = DB::table('round')
            ->select('round.*','c.course_name as course_name','c.zip_code as zip_code')
            ->leftjoin('course as c', 'c.id', 'round.course_id')
			->where('round.id', $id)
            ->orderBy('round.id' , 'desc')->get();
		
        return view('round.view')->with(['data' => $data]);
    }

    public function change_status(Request $request){
        if (!$request->ajax()) { exit('No direct script access allowed'); }
            
            $id = base64_decode($request->id);
            $data = match_round::where(['id' => $id])->first();
       
            if(!empty($data)){
                $update = match_round::where(['id' => $id])->update(['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')]);
                if($update){
                    return match_round()->json(['code' => 200]);
                }else{
                    return match_round()->json(['code' => 201]);
                }
            }else{
                return response()->json(['round' => 201]);
            }
    }


}
