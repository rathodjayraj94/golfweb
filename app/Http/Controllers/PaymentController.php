<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Payment;
use App\Exports\ExportDepartment;
use Maatwebsite\Excel\Facades\Excel;
use Auth, DB, Mail, Validator, File, DataTables, Exception;
use App\Imports\ImportRegion;

class PaymentController extends Controller
{
  

//  index 
    public function index(Request $request){
        if($request->ajax()){
            $data = DB::table('purchase_plan_history')
            ->select('purchase_plan_history.*','m.full_name as full_name','m.whatsapp_no as whatsapp_no','p.plan_name as plan_name','p.plan_amount as plan_amount','p.plan_type as plan_type')
            ->leftjoin('member as m', 'm.id', 'purchase_plan_history.user_id')
            ->leftjoin('plan as p', 'p.id', 'purchase_plan_history.plan_id')
            ->orderBy('purchase_plan_history.id' , 'desc')->get();
            
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($data){
                        $return = '<div class="btn-group">';

                       /* if(auth()->user()->can('city-view')){*/
                           $return .= '<a href="'.route('payment.view',['id' => base64_encode($data->id)]).'" class="btn btn-default btn-xs">
                                            <i class="fa fa-eye"></i>
                                        </a> &nbsp;';
                   
                       //if (auth()->user()->can('city-delete')) {
                            $return .= '<a href="javascript:;" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-bars"></i>
                                            </a> &nbsp;
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="active" data-id="' . base64_encode($data->id) . '">Active</a></li>
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="inactive" data-id="' . base64_encode($data->id) . '">Inactive</a></li>
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="deleted" data-id="' . base64_encode($data->id) . '">Delete</a></li>
                                            </ul>';
                       // }

                        $return .= '</div>';

                        return $return;
                    })

                
                    ->editColumn('status', function ($data) {
                        if ($data->status == 'active') {
                            return '<span class="badge badge-pill badge-success">Active</span>';
                        } else if ($data->status == 'inactive') {
                            return '<span class="badge badge-pill badge-warning">Inactive</span>';
                        } else if ($data->status == 'deleted') {
                            return '<span class="badge badge-pill badge-danger">Deleted</span>';
                        }else{
                            return '-';
                        }
                    })

                    ->rawColumns([ 'action' ,'status'])
                    ->make(true);
            }
        return view('payment.index');
    }

    public function view(Request $request){  
        $id = base64_decode($request->id);
                                
        $data = Payment::select('*')->where(['id' => $id])->first();
        return view('payment.view')->with(['data' => $data]);
    }

    public function change_status(Request $request){
        if (!$request->ajax()) { exit('No direct script access allowed'); }
            
            $id = base64_decode($request->id);
            $data = Payment::where(['id' => $id])->first();
       
            if(!empty($data)){
                $update = Payment::where(['id' => $id])->update(['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')]);
                if($update){
                    return response()->json(['code' => 200]);
                }else{
                    return response()->json(['code' => 201]);
                }
            }else{
                return response()->json(['code' => 201]);
            }
    }


}
