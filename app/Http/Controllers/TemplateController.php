<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth, DB, Mail, Validator, File, DataTables, Exception;
use App\Models\Template;
use App\Imports\ImportDepartment;
use App\Exports\ExportDepartment;
use Maatwebsite\Excel\Facades\Excel;

class TemplateController extends Controller
{
    /** construct */
        public function __construct(){
            $this->middleware('permission:template-create', ['only' => ['create']]);
            $this->middleware('permission:template-edit', ['only' => ['edit']]);
            $this->middleware('permission:template-view', ['only' => ['view']]);
            $this->middleware('permission:template-delete', ['only' => ['delete']]);
        }
    /** construct */

    /** index */
    public function index(Request $request)
    {
       if($request->ajax())
       {
                $data = Template::select('template.*')->where('status','!=','deleted')->orderBy('id' , 'desc')->get();
              
                    return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($data){
                           $return = '<div class="btn-group">';

                            if(auth()->user()->can('template-view')){
                                $return .= '<a href="'.route('template.view', ['id' => base64_encode($data->id)]).'" class="btn btn-default btn-xs">
                                                <i class="fa fa-eye"></i>
                                            </a> &nbsp;';
                            }

                            if(auth()->user()->can('template-edit')){
                                $return .= '<a href="'.route('template.edit', ['id' => base64_encode($data->id)]).'" class="btn btn-default btn-xs">
                                                <i class="fa fa-pencil"></i>
                                            </a> &nbsp;';
                            }

                            if (auth()->user()->can('donor_master-delete')) {
                                $return .= '<a href="javascript:;" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-bars"></i>
                                                </a> &nbsp;
                                                <ul class="dropdown-menu">
                                                    <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="active" data-id="' . base64_encode($data->id) . '">Active</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="inactive" data-id="' . base64_encode($data->id) . '">Inactive</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="deleted" data-id="' . base64_encode($data->id) . '">Delete</a></li>
                                                </ul>';
                            }

                            $return .= '</div>';
                            return $return;
                        })                    
                        ->editColumn('status', function ($data) {
                              if ($data->status == 'active') {
                                 return '<span class="badge badge-pill badge-success">Active</span>';
                           }  else if ($data->status == 'inactive') {
                                 return '<span class="badge badge-pill badge-warning">Inactive</span>';
                           }  else if ($data->status == 'deleted') {
                                 return '<span class="badge badge-pill badge-danger">Deleted</span>';
                           }  else{
                                 return '-';
                            }
                        })

                        ->rawColumns([ 'action' ,'status'])
                        ->make(true);
                }  
       return view('email_template.index'); 
    }
    /** index */

    /** create */
        public function create(Request $request){
            $data = Template::select('template.*')->where('status','!=','deleted')->get();           
            return view('email_template.create')->with(['data' => $data]);
        }
    /** create */

    /** insert */
        public function insert(Request $request){
            if($request->ajax()){
                return true ;
            }
            
            $crud = [                
                'email_data' => $request->email_data ?? '',
                'thank_you_letter' => $request->thank_you_letter ?? '',
                'status' => 'active',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => auth()->user()->id,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => auth()->user()->id
            ];
            
            DB::beginTransaction();
            try {
                DB::enableQueryLog();
                $last_id = Template::insertGetId($crud);
                if ($last_id) {
                    
                    DB::commit();
                    return redirect()->route('template')->with('success', 'Record inserted successfully');
                } else {
                    DB::rollback();
                    return redirect()->back()->with('error', 'Failed to insert record')->withInput();
                }
            } catch (\Throwable $th) {
                DB::rollback();
                return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
            }
        }
    /** insert */

    /** view */
        public function view(Request $request){  
            $id = base64_decode($request->id);
            $data = Template::select('template.*')->where(['id' => $id])->first(); 
            return view('email_template.view')->with(['data' => $data]);
        }
    /** view */

    /** edit */
        public function edit(Request $request){
            $id = base64_decode($request->id);
            $data = Template::select('template.*')->where(['id' => $id])->first();
            return view('email_template.edit')->with(['data' => $data]);
        }
    /** edit */

    /** update */
        public function update(Request $request){
            if($request->ajax()){
                return true ;
            }           

            $crud = [
                'email_data' => $request->email_data ?? '',
                'thank_you_letter' => $request->thank_you_letter ?? '',
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => auth()->user()->id
            ];
           
            
            DB::beginTransaction();
            try {
                DB::enableQueryLog();
                $update = Template::where(['id' => $request->id])->update($crud);
                if($update){
                    DB::commit();
                    return redirect()->route('template')->with('success', 'Record updated successfully');
                } else {
                    DB::rollback();
                    return redirect()->back()->with('error', 'Failed to update record')->withInput();
                }
            } catch (\Throwable $th) {
                DB::rollback();
                return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
            }
        }
    /** update */

    /** change-status */
        public function change_status(Request $request){
            if (!$request->ajax()) { exit('No direct script access allowed'); }
                
                $id = base64_decode($request->id);
                $data = Template::where(['id' => $id])->first();
                
                if(!empty($data)){
                    $update = Template::where(['id' => $id])->update(['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s'), 'updated_by' => auth('sanctum')->user()->id]);
                    if($update){
                        return response()->json(['code' => 200]);
                    }else{
                        return response()->json(['code' => 201]);
                    }
                }else{
                    return response()->json(['code' => 201]);
                }
        }
    /** change-status */

    

}
