<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Branch;
use App\Models\region;
use App\Models\member;
use App\Exports\ExportDepartment;
use Maatwebsite\Excel\Facades\Excel;
use Auth, DB, Mail, Validator, File, DataTables, Exception;
use App\Imports\ImportDepartment;
use App\Models\BusinessCategoriesMaster;
use Illuminate\Support\Str;

class membercontroller extends Controller
{
    public function __construct(){
        /*$this->middleware('permission:city-create', ['only' => ['create']]);
        $this->middleware('permission:city-edit', ['only' => ['edit']]);
        $this->middleware('permission:city-view', ['only' => ['view']]);
        $this->middleware('permission:city-delete', ['only' => ['delete']]);*/
    }
//  construct /

//  index /
    public function index(Request $request){
        if($request->ajax()){
            $data = member::select('*')->orderBy('id' , 'desc')->get();
            
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($data){
                        $return = '<div class="btn-group">';

                
                        $return .= '<a href="'.route('member.edit',['id' => base64_encode($data->id)]).'" class="btn btn-default btn-xs">
                                        <i class="fa fa-pencil"></i>
                                    </a> &nbsp;';
                

                       //if (auth()->user()->can('member-delete')) {
                            $return .= '<a href="javascript:;" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-bars"></i>
                                            </a> &nbsp;
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="active" data-id="' . base64_encode($data->id) . '">Active</a></li>
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="inactive" data-id="' . base64_encode($data->id) . '">Inactive</a></li>
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="deleted" data-id="' . base64_encode($data->id) . '">Delete</a></li>
                                            </ul>';
                       // }

                        $return .= '</div>';

                        return $return;
                    })

                
                    ->editColumn('status', function ($data) {
                        if ($data->status == 'active') {
                            return '<span class="badge badge-pill badge-success">Active</span>';
                        } else if ($data->status == 'inactive') {
                            return '<span class="badge badge-pill badge-warning">Inactive</span>';
                        } else if ($data->status == 'deleted') {
                            return '<span class="badge badge-pill badge-danger">Deleted</span>';
                        }else{
                            return '-';
                        }
                    })

                    ->rawColumns([ 'action' ,'status'])
                    ->make(true);
            }
        return view('member.index');
    }
// index /

// create /
    public function create(Request $request){
        
        return view('member.create');
    }
//  create /

//  insert /
    public function insert(Request $request){
        if($request->ajax()){
            return true ;
        }
       
        if($request->file('image'))
        {
            $file = $request->file('image');
            $filename = time() . '.' . $request->file('image')->extension();
            $filePath = public_path() . '/images/';
            $file->move($filePath, $filename);
        }
        else
        {
            $filename = '';
        }
        
        $countmember = member::select('*')->count();
        $countmember = $countmember + 1;
        $randomNumber = str_pad($countmember,5, "0", STR_PAD_LEFT);  
        
        $crud = [
            'status' => 'active',
            'image'=> $filename,
            'full_name' => $request->full_name,
            'whatsapp_no'=>$request->whatsapp_no,
            'email_id'=>$request->email_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
        
        $token = Str::random(64);
        Mail::send('template.registermail', ['token' => $token], function($message) use($request){
            $message->to($request->email_id);
            $message->subject('Register Successfully');
        });
        
        DB::beginTransaction();
        try {
            DB::enableQueryLog();
            $last_id = member::insertGetId($crud);
            if ($last_id) {
                
                DB::commit();
                return redirect()->route('member')->with('success', 'Record inserted successfully');
            } else {
                DB::rollback();
                return redirect()->back()->with('error', 'Failed to insert record')->withInput();
            }
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
        }
    }
//  insert /

//  view /
    public function view(Request $request)
    {  
        $id = base64_decode($request->id);
                                
        $data = member::select('*')->where(['id' => $id])->first();
        $country = member::select('member.*')->where(['status'=>'active'])->get(); 
        return view('member.view')->with(['data' => $data,'country'=>$country]);
    }
    public function edit(Request $request)
    {
        $id = base64_decode($request->id);
                                
        $data = member::select('*')->where(['id' => $id])->first();
        return view('member.edit')->with(array('data' => $data));
    }
    public function update(Request $request)
    {
        if($request->ajax()){
            return true ;
        }
       // $branch_admin=$request->branch_admin ? 1 : NULL;
        if($request->file('image'))
        {
            $file = $request->file('image');
            $filename = time() . '.' . $request->file('image')->extension();
            $filePath = public_path() . '/images/';
            $file->move($filePath, $filename);
        }
        else
        {
            $filename = $request->oldlogo;
        }
     
        $crud = [
            'status' => 'active',
            'image'=> $filename,
            'full_name' => $request->full_name,
            'whatsapp_no'=>$request->whatsapp_no,
            'email_id'=>$request->email_id,
            'updated_at' => date('Y-m-d H:i:s'),
        ];
       
    
              
        DB::beginTransaction();
        try {
            DB::enableQueryLog();
            $update = member::where(['id' => $request->id])->update($crud);
            if($update){
                DB::commit();
                return redirect()->route('member')->with('success', 'Record updated successfully');
            } else {
                DB::rollback();
                return redirect()->back()->with('error', 'Failed to update record')->withInput();
            }
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
        }
    }
//  update /

// change-status /
    public function change_status(Request $request){
        if (!$request->ajax()) { exit('No direct script access allowed'); }
            
            $id = base64_decode($request->id);
            $data = member::where(['id' => $id])->first();
       
            if(!empty($data)){
                $update = member::where(['id' => $id])->update(['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')]);
                if($update){
                    return response()->json(['code' => 200]);
                }else{
                    return response()->json(['code' => 201]);
                }
            }else{
                return response()->json(['code' => 201]);
            }
    }
//  change-status /

/* Export /
   /* public function export(Request $request){
        if(isset($request->slug) && $request->slug != null){
            $slug = $request->slug;
        }else{
            $slug = 'all';
        }

        $name = 'all_city_'.Date('YmdHis').'.xlsx';

        try {
            return Excel::download(new ExportDepartment($slug), $name);
        }catch(\Exception $e){
            return redirect()->back()->with('error', $e->getMessage());
        }
    }*/
/* Export /
/* Import /
/*public function import(Request $request){
       
    if(Excel::import(new ImportDepartment, $request->file('file')->store('file'))){
        return redirect()->route('department')->with('sucess' ,'File Imported Sucessfully');
    }else{
        return redirect()->route('department')->with('error' ,'Faild To Import File!');
    }
}*/
// Import /
}