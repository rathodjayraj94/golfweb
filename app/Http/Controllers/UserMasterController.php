<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Storage;
use App\Models\UsersMaster;
use App\Imports\ImportUser;
use App\Exports\ExportUser;
use App\Http\Requests\UserRequest;
use Maatwebsite\Excel\Facades\Excel;
use Auth, DB, Mail, Validator, File, DataTables, Exception;

class UserMasterController extends Controller{
    /** construct */
        public function __construct(){
           /* $this->middleware('permission:user_master-create', ['only' => ['create']]);
            $this->middleware('permission:user_master-edit', ['only' => ['edit']]);
            $this->middleware('permission:user_master-view', ['only' => ['view']]);
            $this->middleware('permission:user_master-delete', ['only' => ['delete']]);*/
        }
    /** construct */

    /** index */
        public function index(Request $request)
        {
            if ($request->ajax()) {
                $data = UsersMaster::orderBy('id', 'desc')->get();


                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($data) {
                        $return = '<div class="btn-group">';


                            $return .=  '<a href="'.route('user_master.view', ['id' => base64_encode($data->id)]).'" class="btn btn-default btn-xs">
                                                <i class="fa fa-eye"></i>
                                            </a> &nbsp;';



                            $return .= '<a href="'.route('user_master.edit', ['id' => base64_encode($data->id)]).'" class="btn btn-default btn-xs">
                                                <i class="fa fa-edit"></i>
                                            </a> &nbsp;';


                            $return .= '<a href="javascript:;" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-bars"></i>
                                        </a> &nbsp;
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="active" data-id="' . base64_encode($data->id) . '">Active</a></li>
                                            <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="inactive" data-id="' . base64_encode($data->id) . '">Inactive</a></li>
                                            <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="deleted" data-id="' . base64_encode($data->id) . '">Delete</a></li>
                                        </ul>';
                  //  }

                    $return .= '</div>';


                        return $return;
                    })

                    ->editColumn('status', function ($data) {

                        if ($data->status == 'active') {
                            return '<span class="badge badge-pill badge-success">Active</span>';
                        } else if ($data->status == 'inactive') {
                            return '<span class="badge badge-pill badge-warning">Inactive</span>';
                        } else if ($data->status == 'deleted') {
                            return '<span class="badge badge-pill badge-danger">Deleted</span>';
                        } else {
                            return '-';
                        }
                    })



                    ->rawColumns([ 'action', 'status'])
                    ->make(true);
            }

            return view('user_master.index');
        }
    /** index */



    /** create */
        public function create(Request $request){
            $data = UsersMaster::select('user_master.*')->where(['status' => 'active'])->get();
            return view('user_master.create')->with(['data' => $data,'department'=>$department,'designation'=>$designation]);
        }
    /** create */

    /** insert */
        public function insert(Request $request)
        {
            if($request->ajax()){ return true; }





            if($request->hasFile('photo')){
                $photo = $request->photo;
                $file_name = $photo->getClientOriginalName();
                $folder = public_path('\uploads\user_master'.'/'. $request->$file_name);
                if (!File::exists($folder)) {
                    File::makeDirectory($folder, 0775, true, true);
                }
                //$dir = public_path().'\uploads\donar_master_kyc';

                $file = $photo->move($folder, $file_name);
            }
            $data = [

                'name' => $request->name,
                'department' => $request->department,
                'designation' => $request->designation,
                'phone_number' => $request->phone_number,
                'email' => $request->email,
                'address' => $request->address,
                'country' => $request->country,
                'state' => $request->state,
                'city' => $request->city,
                'permission_management' => $request->permission_management,
                'role' => $request->role,
                'photo' => $request->photo,
                'status' => 'active',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => auth()->user()->id,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => auth()->user()->id

            ];





            DB::beginTransaction();
            try {
                $UsersMaster = UsersMaster::create($data);
             //   $UsersMaster->assignRole($request->role);

                if($UsersMaster){
                    DB::commit();
                    return redirect()->route('user_master')->with('success', 'Record inserted successfully');
                } else {
                    DB::rollback();
                    return redirect()->back()->with('error', 'Failed to insert record')->withInput();
                }
            } catch (\Throwable $th) {
                DB::rollback();
                return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
            }
            }
    /** insert */

    /** edit */
        public function edit(Request $request, $id = ''){
            if (isset($id) && $id != '' && $id != null)
                $id = base64_decode($id);
            else
                return redirect()->route('user_master')->with('error', 'Something went wrong');
                $roles = Role::all();
             //   $data = UsersMaster::select('user_master.*')->where(['status' => 'active'])->get();
                $designation = UsersMaster::select('user_master.*')->where(['status'=>'active'])->get();
                $department = UsersMaster::select('user_master.*')->where(['status'=>'active'])->get();

            // $branch = Branch::where(['status' => 'active'])->get();
            $data = UsersMaster::select('*')->where(['id' => $id])->first();
     //      $data = UsersMaster::select('*')->where(['id' => '1'])->first();
            return view('user_master.edit')->with(['data' => $data,'department'=>$department,'designation'=>$designation]);
        }
    /** edit */

    /** update */
        public function update(Request $request){
            if($request->ajax()) { return true; }

            if($request->hasFile('photo')){
                $photo = $request->photo;
                $file_name = $photo->getClientOriginalName();
                $folder = public_path('\uploads\user_master'.'/'. $request->$file_name);
                if (!File::exists($folder)) {
                    File::makeDirectory($folder, 0775, true, true);
                }
                //$dir = public_path().'\uploads\donar_master_kyc';

                $file = $photo->move($folder, $file_name);
            }
        //    $id = $request->id;
        //    $exst_rec = UsersMaster::where(['id' => $id])->first();

            $data = [

                'name' => $request->name,
                'department' => $request->department,
                'designation' => $request->designation,
                'phone_number' => $request->phone_number,
                'email' => $request->email,
                'address' => $request->address,
                'country' => $request->country,
                'state' => $request->state,
                'city' => $request->city,
                'permission_management' => $request->permission_management,
                'role' => $request->role,
                'photo' => $request->photo,

                'status' => 'active',

                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => auth()->user()->id,

            ];

            // if($request->password != '' && $request->password != NULL)
            //     $data['password'] = $request->password;

            DB::beginTransaction();
            try {
           //     $update = UsersMaster::where(['id' => $id])->update($data);
                $update = UsersMaster::where(['id' => $request->id])->update($data);
                if ($update) {
               //     DB::table('model_has_roles')->where(['model_id' => $id])->delete();

              //     $exst_rec->assignRole($request->role);

                    DB::commit();
                    return redirect()->route('user_master')->with('success', 'Record updated successfully');
                } else {
                    DB::rollback();
                    return redirect()->back()->with('error', 'Failed to update record')->withInput();
                }
            } catch (\Throwable $th) {
                DB::rollback();
                return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
            }
        }
    /** update */

    /** view */
        public function view(Request $request, $id = ''){


            if (isset($id) && $id != '' && $id != null)
                $id = base64_decode($id);
            else
                return redirect()->route('user_master')->with(['data' => $data]);

           // $roles = Role::all();
            // $branch = Branch::where(['status' => 'active'])->get();

            // $data = UsersMaster::select('*')
            //                     ->where(['id' => $id])
            //                     ->first();

            $designation = UsersMaster::select('user_master.*')->where(['status'=>'active'])->get();
            $department = UsersMaster::select('user_master.*')->where(['status'=>'active'])->get();

            $data = UsersMaster::select('*')->where(['id' => $id])->first();
            return view('user_master.view')->with(['data' => $data,'department'=>$department,'designation'=>$designation]);
        }
    /** view */

    /** change-status */
        public function change_status(Request $request){
            if (!$request->ajax()) { exit('No direct script access allowed'); }

            if (!empty($request->all())) {
                $id = base64_decode($request->id);
                $status = $request->status;

                $data = UsersMaster::where(['id' => $id])->first();

                if(!empty($data)){
                    $process = UsersMaster::where(['id' => $id])->update(['status' => $status, 'updated_by' => auth()->user()->id]);
                    if($process){
                        return response()->json(['code' => 200]);
                    }else{
                        return response()->json(['code' => 201]);
                    }
                } else {
                    return response()->json(['code' => 201]);
                }
            } else {
                return response()->json(['code' => 201]);
            }
        }
    /** change-status */

    /** Import */
      /*  public function import(Request $request){

            if(Excel::import(new ImportUser, $request->file('file')->store('file'))){
                return redirect()->route('user_master')->with('sucess' ,'File Imported Sucessfully');
            }else{
                return redirect()->route('user_master')->with('error' ,'Faild To Import File!');
            }
        }*/
    /** Import */

    /** Export */
        /*public function export(Request $request){
            if(isset($request->slug) && $request->slug != null){
                $slug = $request->slug;
            }else{
                $slug = 'all';
            }

            $name = 'Users_'.Date('YmdHis').'.xlsx';

            try {
                return Excel::download(new ExportUser($slug), $name);
            }catch(\Exception $e){
                return redirect()->back()->with('error' ,$e->getMessage());
            }
        }*/
    /** Export */
}
