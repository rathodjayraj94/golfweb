<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth, DB, Validator, File;
use App\Models\member;
use App\Models\guest;


class AuthController extends Controller
{   
       
    /** login */
        public function login(Request $request)
        {

              $customer = guest::where('whatsapp_number', $request->mobile_number)->get();
             
                if(isset($customer[0]->status))
                {
                    if($customer[0]->status == 'active')
                    {
                        return response()->json(['status' => 200, 'message' => 'Login Successfully','detail' => $customer]);
                    }
                    else
                    {
                        return response()->json(['status' => 201, 'message' => 'Customer Inactive']);

                    }   
                }
                else
                {
                    return response()->json(['status' => 201, 'message' => 'Customer not found']);

                }
           // $rules = ['email' => 'required', 'password' => 'required'];
           // $validator = Validator::make($request->all(), $rules);
            
            /*if($validator->fails())
                return response()->json(['status' => 422, 'message' => $validator->errors()]);
            $auth = auth()->attempt(['email' => $request->email, 'password' => $request->password]);
            
            if(!$auth){
                return response()->json(['status' => 401, 'message' => 'Invalid login details']);
            }else{
                $user = guest::where('whatsapp_number', $request->mobile_number)->firstOrFail();
                
                if($user->status == 'active'){
                    $token = $user->createToken('auth_token')->plainTextToken;
                    $u = \Auth::user(); // get the authenticated User
                    $u->load('roles.permissions');
                    return response()->json(['status' => 200, 'message' => 'Login Successfully', 'token_type' => 'Bearer', 'access_token' => $token ,'permission' => $u]);
                }else{
                    return response()->json(['status' => 201, 'message' => 'This account has been inactive or deleted, please contact admin']);
                }
            } */
           
        }

        
        public function memberlogin(Request $request){

            $member = member::where('whatsapp_no', $request->mobile_number)->get();
           
            if(isset($member[0]->status))
            {
                if($member[0]->status == 'active')
                {
                    return response()->json(['status' => 200, 'message' => 'Login Successfully','detail' => $member]);
                }
                else
                {
                    return response()->json(['status' => 201, 'message' => 'Login Member Inactive']);

                }   
            }
            else
            {
                return response()->json(['status' => 201, 'message' => 'Member not found']);

            }
          
        }   
    /** login */
        
    /** logout */
        public function logout(Request $request){
            $request->user()->currentAccessToken()->delete();
            return response()->json(['status' => 200, 'message' => 'Logout Successfully']);
        }
    /** logout */
}
