<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth,DB, Mail, Validator;
use App\Models\member;
use Illuminate\Support\Str;
use Carbon\Carbon;


class GolfController extends Controller
{
    /** construct */
        public function __construct(){
            $this->middleware('permission:categories-create', ['only' => ['create']]);
            $this->middleware('permission:categories-edit', ['only' => ['edit']]);
            $this->middleware('permission:categories-view', ['only' => ['view']]);
            $this->middleware('permission:categories-delete', ['only' => ['delete']]);
        }
    /** construct */
    /** index */
        public function index(Request $request)
        {
            $data = DB::table('member')->where('member.status','active')->orderBy('member.id', 'desc')->get();
            if (sizeof($data))
            {
                return response()->json(['status' => 200, 'message' => 'Data Found.', 'Data' => $data]);
            }
            else
            {
                return response()->json(['status' => 404, 'message' => 'No Data Found.']);
            }
          
        }
		public function clublist(Request $request)
        {
            $data = DB::table('club_master')->where('status','active')->orderBy('id', 'desc')->get();
            if (sizeof($data))
            {
                return response()->json(['status' => 200, 'message' => 'Club Data Found.', 'Data' => $data]);
            }
            else
            {
                return response()->json(['status' => 404, 'message' => 'Club  No Data Found.']);
            }
        
        }
		public function plan(Request $request)
        {
            
              $data = DB::table('plan')->where('plan.status','active')->orderBy('plan.id', 'desc')->get();
            $dataactive = DB::table('purchase_plan_history')->where('expired_at', '>=', Carbon::today())->where('purchase_plan_history.status','active')->where('purchase_plan_history.user_id',$request->user_id)->orderBy('purchase_plan_history.id', 'desc')->first();
            $previous = DB::table('purchase_plan_history')->where('expired_at', '<=', Carbon::today())->where('purchase_plan_history.status','active')->where('purchase_plan_history.user_id',$request->user_id)->orderBy('purchase_plan_history.id', 'desc')->first();
           
            $totalactive = DB::table('purchase_plan_history')->where('expired_at', '>=', Carbon::today())->where('purchase_plan_history.status','active')->where('purchase_plan_history.user_id',$request->user_id)->orderBy('purchase_plan_history.id', 'desc')->count();
            $trial_check = DB::table('purchase_plan_history')->select('*')->where('plan_id','=', 1)->where('user_id',"=",$request->user_id)->count();
               $msg = array();
             foreach($data as $value)
            {
               

                $msg[] = array(
                    "id"	=> (int)$value->id,
                    "plan_name" => $value ->plan_name,
                    "plan_amount"   => (string)$value->plan_amount,	
                    "days" => $value->days,
                    "plan_type"   => $value->plan_type,	
                    "status"   => $value->status,
                    "created_at" =>  $value->created_at,
                    "updated_at" =>  $value->updated_at,
                    "trial_check" =>  $trial_check
                    
                );
            }	
            
            if(sizeof($data))
            {
                return response()->json(['status' => 200, 'message' => 'Data Found.', 'Data' => $msg,'Plan' => $dataactive,'previous' => $previous,'total_active' => $totalactive ]);
            }
            else
            {
                return response()->json(['status' => 404, 'message' => 'No Data Found.']);
            }
        
        }
		
        public function updateprofileimage(Request $request)
        {
            if($request->file('image'))
            {
                    $file = $request->file('image');
                    $filename = time() . '.' . $request->file('image')->extension();
                    $filePath = public_path() . '/images/';
                    $file->move($filePath, $filename);
    
                    $crud = [
                        'image'=> $filename,
                        'updated_at' => date('Y-m-d H:i:s')
                        ];	
                        
                    $update = member::where(['id' => $request->id])->update($crud);
                    
                    if($update)
                    {
                        return response()->json(['status' => 200, 'message' => 'Image Update Successfully','Image' => $filename]);
                    }
                    else
                    {
                        return response()->json(['status' => 201, 'message' => ' Image Not Update Successfully']);
                    }
            }
  
        }


        public function updatepassword(Request $request)
        {
            $user_id = $request->user_id;
            $currentpassword = $request->current_password;
            $currentpass = md5( $currentpassword);

            $password = $request->password;

            $pass = md5($password);
            $data = DB::table('member')->where('password',$currentpass)->where('id',$user_id)->count();

            if($data != 0)
            {
                $crud = [
                    'password'=> $pass,
                    'updated_at' => date('Y-m-d H:i:s')
                    ];	
                    
                $update = member::where(['id' => $request->id])->update($crud);
                
                if($update)
                {
                    return response()->json(['status' => 200, 'message' => 'Password Update Successfully']);
                }
                else
                {
                    return response()->json(['status' => 201, 'message' => ' Password Not Update Successfully']);
                }
            }
            else
            {
                return response()->json(['status' => 201, 'message' => ' Current Password Not match']);
            }
        }

        public function zipcode(Request $request)
        {
            $data = DB::table('golfcourses')->where('zip', $request->zip_code)->orderBy('course_id', 'desc')->get();
            $msg = array();
            foreach($data as $value)
            {
               

                $msg[] = array(
                    "id"	=> (int)$value->course_id,
                    "course_name" => $value ->coursename,
                    "zip_code"   => $value->zip,	
                    "total_hole" => $value->holes,
                    "lat"   => $value->latitude,	
                    "long"   => $value->longitude		
                    );
            }	
            
            if (sizeof($data)) {
                return response()->json(['status' => 200, 'message' => 'Data Found.', 'Data' => $msg]);
            } else {
                return response()->json(['status' => 404, 'message' => 'No Data Found.']);
            }
          
        }
        public function latlong(Request $request)
        {
            
            $latitude = $request->lat;
            $longitude = $request->long;
            
             
            $latitude = round($latitude,4);
	        $longitude = round($longitude,4);
	        
            $data = DB::table("golfcourses")->select("golfcourses.course_id","coursename","zip","holes","latitude","longitude",DB::raw("course_id, ( 6371  * acos( cos( radians('$latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians( latitude ) ) ) ) AS distance"))->havingRaw('distance <=25')->orderBy('distance')->limit(120)->get();
            //$data = DB::table('golfcourses')->where('latitude', $request->lat)->where('longitude', $request->long)->orderBy('course_id', 'desc')->get();
            $msg = array();
            foreach($data as $value)
            {
               

                $msg[] = array(
                    "id"	=> (int)$value->course_id,
                    "course_name" => $value ->coursename,
                    "zip_code"   => $value->zip,	
                    "total_hole" => $value->holes,
                    "lat"   => $value->latitude,	
                    "long"   => $value->longitude		
                    );
            }	
            
            if (sizeof($data)) {
                return response()->json(['status' => 200, 'message' => 'Data Found.', 'Data' => $data]);
            } else {
                return response()->json(['status' => 404, 'message' => 'No Data Found.']);
            }
        }

        public function course_detail(Request $request)
        {
            $data = DB::table('golfcourses')->where('course_id', $request->course_id)->orderBy('course_id', 'desc')->get();
            $msg = array();
            $coursename='';
            $coursezip='';
            foreach($data as $value)
            {
               $coursename =  $value ->coursename;
               $coursezip = $value->zip;

                $msg = array(
                    "id" => (string)$value->course_id,
                    "course_name" => $value ->coursename,
                    "zip_code"   => $value->zip,	
                    "total_hole" => $value->holes,
                    "lat"   => $value->latitude,	
                    "long"   => $value->longitude		
                    );
            }
            
            $holedata = DB::table('golfholes')->where("course_zip",$coursezip)->where('course_name', $coursename)->orderByRaw("CAST(hole as UNSIGNED) ASC")->get();
            $hole_array = array();
          
            foreach($holedata as $value)
            {
                $videodata = DB::table('wireframe_hubspoke')->where('hole_no',$value->hole)->where('course_name', $coursename)->first();
                
                if(isset($videodata->hole_flyover_url))
                {
                    $hole_flyover_url = $videodata->hole_flyover_url;
                }
                else
                {
                    $hole_flyover_url = '';

                }    
                $hole_array[] = array(
                    "course_name"	=> $value->course_name,
                    "hole" => $value ->hole,
                    "pro_par"   => $value->pro_par,	
                    "pro_yards" => $value->pro_yards,
                    "pro_HCP"   => $value->pro_HCP,
                    "champ_par"   => $value->champ_par,	
                    "champ_yards" => $value->champ_yards,
                    "champ_HCP"   => $value->champ_HCP,	
                    "mens_par"   => $value->mens_par,	
                    "mens_yards" => $value->mens_yards,
                    "mens_HCP"   => $value->mens_HCP,
                    "womens_par"   => $value->womens_par,	
                    "womens_yards" => $value->womens_yards,
                    "womens_HCP"   => $value->womens_HCP,
                    "juniors_par"   => $value->juniors_par,	
                    "juniors_yards" => $value->juniors_yards,
                    "juniors_HCP"   => $value->juniors_HCP,	
                    "hole_tee_target_latitude"   => $value->hole_tee_target_latitude,		
                    "hole_tee_target_longitude"   => $value->hole_tee_target_longitude,		
                    "hole_greenfront_latitude"   => $value->hole_greenfront_latitude,		
                    "hole_greenfront_longitude"   => $value->hole_greenfront_longitude,		
                    "hole_greenmiddle_latitude"   => $value->hole_greenmiddle_latitude,		
                    "hole_greenmiddle_longitude"   => $value->hole_greenmiddle_longitude,	
                    "hole_greenback_latitude"   => $value->hole_greenback_latitude,		
                    "hole_greenback_longitude"   => $value->hole_greenback_longitude,
                    'hole_flyover_url' => $hole_flyover_url
                    );
            }
                
            $landdata = DB::table('Landmarks')->where('CourseName', $coursename)->orderBy('Hole', 'ASC')->get();
            $land_array = array();
            foreach($landdata as $value)
            {

                $land_array[] = array(
                    "CourseName" => $value->CourseName,
                    "CourseZip" => $value ->CourseZip,
                    "Hole"   => $value->Hole,	
                    "landmark_name" => $value->landmark_name,
                    "hole_landmark_position"   => $value->hole_landmark_position,
                    "hole_landmark_layup_lattitude"   => $value->hole_landmark_layup_lattitude,
                    "hole_landmark_layup_longitude"   => $value->hole_landmark_layup_longitude,
                    "hole_landmark_carry_lattitude"   => $value->hole_landmark_carry_lattitude,
                    "hole_landmark_carry_longitude"   => $value->hole_landmark_carry_longitude
                );  
                

            }

            $new_arrayvalu[]= array("hole" => $hole_array,"data" => $land_array);
            
            
            if (sizeof($data)) {
                return response()->json(['status' => 200, 'message' => 'Data Found.', 'course' => $msg,'course_detail' => $new_arrayvalu]);
            } else {
                return response()->json(['status' => 404, 'message' => 'No Data Found.']);
            }
        }

        public function user_club(Request $request)
        {
            $data = DB::table('user_club')->select('club_id')->where('id', $request->user_id)->orderBy('id', 'desc')->get();
            if (sizeof($data)) {
                return response()->json(['status' => 200, 'message' => 'Data Found.', 'Data' => $data]);
            } else {
                return response()->json(['status' => 404, 'message' => 'No Data Found.']);
            }
        }

        public function user_club_save(Request $request)
        {
            $crud = [
                'user_id' => $request->user_id,
                'club_id' => $request->club_id,
                'created_at' => date('Y-m-d H:i:s')  
            ];   


            $count = DB::table('user_club')->where('user_id', $request->user_id)->count();
            if($count == 0)
            {
                $values = array(  $crud);
                DB::table('user_club')->insert($values);
            }
            else
            {
                DB::table('user_club')->where('user_id', '=', $request->user_id)->update( $crud);
            }
             return response()->json(['status' => 200, 'message' => 'User Clublist Updated']);

        }   


        public function user_plan_purchase(Request $request)
        {
            $plan_Day = DB::table('plan')->select('id','days','plan_name','plan_type',"plan_amount")->where('id', '=', $request->plan_id)->first();
            $days =  $plan_Day->days;
            $planid =  $plan_Day->id;
            $plan_name = $plan_Day->plan_name;
            $plan_type = $plan_Day->plan_type;
            $plan_amount = $plan_Day->plan_amount;
            $countuser = DB::table('purchase_plan_history')->select('*')->where('plan_id','=', $request->plan_id)->where('user_id',"=",$request->user_id)->count();
            
            
            $crud = [
                'user_id' => $request->user_id,
                'plan_id' => $request->plan_id,
                'transaction' => $request->transaction,
                'payment_status' =>  'Success',
                'status' =>  'active',
                'created_at' => date('Y-m-d H:i:s')  
            ];   
    
            if($plan_type == "free")
            {
                if($countuser == 0)
                {
                    
                   
                    $data = DB::table('purchase_plan_history')->insertGetId($crud);
                    $plan_Day = DB::table('plan')->select('days')->where('id', '=', $request->plan_id)->first();
                    $days =  $plan_Day->days;
                    $newDateTime = Carbon::now()->addDays($days);
        
                    $crudd = [
                        'expired_at'=> $newDateTime,
                        'updated_at' => date('Y-m-d H:i:s')
                        ];	
                        
                    $update = DB::table('purchase_plan_history')->where(['id' => $data])->update($crudd);
        
                    return response()->json(['status' => 200, 'message' => 'User Plan Active Successfully']);
                
                }
                else
                {
                     return response()->json(['status' => 400, 'message' => 'You already purchased trial version. ']);
            
                }    
            }   
            else
            {
                $data = DB::table('purchase_plan_history')->insertGetId($crud);
                $plan_Day = DB::table('plan')->select('days')->where('id', '=', $request->plan_id)->first();
                $days =  $plan_Day->days;
                $newDateTime = Carbon::now()->addDays($days);
    
                $crudd = [
                    'expired_at'=> $newDateTime,
                    'updated_at' => date('Y-m-d H:i:s')
                    ];	
                    
                $update = DB::table('purchase_plan_history')->where(['id' => $data])->update($crudd);
    
                return response()->json(['status' => 200, 'message' => 'User Plan Active Successfully']);
        
            }        
        }    

        public function user_course_save(Request $request)
        {
            $crud = [
                'user_id' => $request->user_id,
                'course_id' => $request->course_id,
                'created_at' => date('Y-m-d H:i:s')  
            ];   

            $data = DB::table('user_course')->insert($crud);

            return response()->json(['status' => 200, 'message' => 'User Course Added Successfully']);
        }

        public function user_delete_course(Request $request)
        {
            if(isset($request->id))
            {
                 DB::table('user_course')->where('id', $request->id)->delete();

                 return response()->json(['status' => 200, 'message' => ' Course Deleted Successfully']);

            }
            else
            {
                return response()->json(['status' => 200, 'message' => ' Something Wrong']);

            }


        }


        public function search_member(Request $request)
        {
            $data = DB::table('member')->where('member.status','!=','deleted')->where('member.whatsapp_no',$request->search)->orWhere('member.email_id',$request->search)->orderBy('member.id' , 'desc')->get();
            if(sizeof($data))
            {
                return response()->json(['status' => 200, 'message' => 'Successfully Recored','Data' => $data]);
            }
            else
            {
                return response()->json(['status' => 200, 'message' => 'No Data Found']);
 
            }    
        }
        
        public function add_guest(Request $request)
        {
            $crud = [
                'name' => $request->name,
                'email' => $request->email,
                'created_at' => date('Y-m-d H:i:s')  
            ];   

            $data = DB::table('guest')->insertGetId($crud);
            return response()->json(['status' => 200, 'message' => 'Guest Successfully Recored','id' => $data]);
 
        }

        public function add_comment(Request $request)
        {
            $crud = [
                'round_id' => $request->round_id,
                'user_id' => $request->user_id,
                'comment' => $request->comment,
                'created_at' => date('Y-m-d H:i:s')  
            ];   

            $data = DB::table('comment')->insertGetId($crud);
            return response()->json(['status' => 200, 'message' => 'Comment Successfully added']);
 
        }

        public function add_round(Request $request)
        {
            $course_id = $request->course_id;
            $crud = [
                'course_id' => $course_id,
                'status' => 'active',
                'created_at' => date('Y-m-d H:i:s')  
            ];   

            $data = DB::table('round')->insertGetId($crud);

             $user_id = explode(",",$request->user_id);
            $guest_id = explode(",",$request->guest_id);
            if(!empty($user_id))
            {
                foreach($user_id as $user)
                {
                    if($user != null)
                    {
                        $crudd = [
                            'round_id' => $data,
                            'course_id' => $course_id,
                            'user_id' => $user,
                            'created_at' => date('Y-m-d H:i:s')  
                        ];   
            
                        $datad = DB::table('match_round_user')->insert($crudd);
                    }
                }
            }
            if(!empty($guest_id))
            {
                foreach($guest_id as $guest)
                {
                    if($guest != null)
                    {
                    $guestt = [
                        'round_id' => $data,
                        'course_id' => $course_id,
                        'guest_id' => $guest,
                        'created_at' => date('Y-m-d H:i:s')  
                    ];  
                    
        
                    $datad = DB::table('match_round_user')->insert($guestt);
                
                    }        
                }
            }

            return response()->json(['status' => 200, 'message' => 'Round Successfully Created','id' => $data]);
 
        }

        public function my_round(Request $request)
        {
              
            $latitude = $request->lat;
            $longitude = $request->long;
            
            $latitude = round($latitude,4);
	        $longitude = round($longitude,4);
	
            if(isset($latitude))
            {

            
                  $dataa = DB::table('match_round_user')->select('match_round_user.*','c.coursename as course_name','c.course_id as course_id','c.zip as zip_code',"holes","latitude","longitude",DB::raw("c.course_id, ( 6371  * acos( cos( radians('$latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians( latitude ) ) ) ) AS distance"))
            ->join('golfcourses as c', 'c.course_id', 'match_round_user.course_id')
            ->havingRaw('distance < 25')
            ->where('match_round_user.user_id', $request->user_id)
            ->orderBy('distance')->limit(120)->get();
                
            }
            else
            {
            $dataa = DB::table('match_round_user')->select('match_round_user.*','c.coursename as course_name','c.zip as zip_code')
            ->join('golfcourses as c', 'c.course_id', 'match_round_user.course_id')
            ->where('match_round_user.user_id', $request->user_id)
            ->orderBy('match_round_user.id' , 'desc')->get();
            }
            
            if(sizeof($dataa))
            {
                return response()->json(['status' => 200, 'message' => 'Data Found.', 'Data' => $dataa]);
            }
            else
            {
                return response()->json(['status' => 404, 'message' => 'No Round Data Found.']);
            }
         

        }
        
         public function commentlist(Request $request)
        {
            $data = DB::table('comment')
            ->select('comment.*','m.full_name as full_name','m.image')
            ->leftjoin('member as m', 'm.id', 'comment.user_id')
            ->where('comment.round_id', $request->round_id)
            ->orderBy('comment.id' , 'desc')->get();
            if (sizeof($data))
            {
                return response()->json(['status' => 200, 'message' => 'Commentlist Data Found.', 'Data' => $data]);
            }
            else
            {
                return response()->json(['status' => 404, 'message' => 'No Round Data Found.']);
            }    
        }
        
         public function add_rating(Request $request)
        {
            $crud = [
                'user_id' => $request->user_id,
                'course_id' => $request->course_id,
                'rate' => $request->rate,
                'rate_comment' => $request->rate_comment,
                'status' => 'active',
                'created_at' => date('Y-m-d H:i:s')  
            ];   

            $data = DB::table('rating')->insertGetId($crud);
            return response()->json(['status' => 200, 'message' => 'Rating Successfully']);
 
        }
        
       public function tee_master(Request $request)
		{
			$data = DB::table('tee_master')->where('status','active')->orderBy('id', 'asc')->get();
            if (sizeof($data))
            {
                return response()->json(['status' => 200, 'message' => 'Tee Master Data Found.', 'Data' => $data]);
            }
            else
            {
                return response()->json(['status' => 404, 'message' => 'Club  No Data Found.']);
            }
		}
		
		
        public function add_scrore(Request $request)
        {
           if($request->type == "strokes")
            {
                $crud = [
                    'strokes'=> $request->score,
                    'hole' => $request->hole,
                    'updated_at' => date('Y-m-d H:i:s')
                    ];	
                    
                $update = DB::table('match_round_user')->where(['round_id' => $request->round_id])->where(['user_id' => $request->user_id])->update($crud);
                if($update)
                {
                     $crud = [
                        'user_id' => $request->user_id,
                        'round_id' => $request->round_id,
                        'type' => 'strokes',
                        'score' => $request->score,
                        'hole' => $request->hole,
                        'created_at' => date('Y-m-d H:i:s')  
                    ];   
        
                    $data = DB::table('match_histroy')->insertGetId($crud);
                    
                    return response()->json(['status' => 200, 'message' => 'User Score Update Successfully','data' => $crud]);
                }
                else
                {
                    return response()->json(['status' => 200, 'message' => 'No Score Update Successfully']);
                }
            }
            else
            {
                $crud = [
                    'putts' => $request->score,
                    'hole' => $request->hole,
                    'updated_at' => date('Y-m-d H:i:s')
                    ];	
                    
                $update = DB::table('match_round_user')->where(['round_id' => $request->round_id])->where(['user_id' => $request->user_id])->update($crud);
                if($update)
                {
                     $crud = [
                        'user_id' => $request->user_id,
                        'round_id' => $request->round_id,
                        'type' => 'putts',
                        'score' => $request->score,
                        'hole' => $request->hole,
                        'created_at' => date('Y-m-d H:i:s')  
                    ];   
        
                    $data = DB::table('match_histroy')->insertGetId($crud);
                    
                    return response()->json(['status' => 200, 'message' => 'User Score Update Successfully','data' => $crud]);
                }
                else
                {
                    return response()->json(['status' => 200, 'message' => 'No Score Update Successfully']);
                }
            }
        }
        
        public function add_guest_scrore(Request $request)
        {
           if($request->type == "strokes")
            {
                $crud = [
                    'strokes'=> $request->score,
                    'hole' => $request->hole,
                    'updated_at' => date('Y-m-d H:i:s')
                    ];	
                    
                $update = DB::table('match_round_user')->where(['round_id' => $request->round_id])->where(['guest_id' => $request->user_id])->update($crud);
                if($update)
                {
                     $crud = [
                        'guest_id' => $request->user_id,
                        'round_id' => $request->round_id,
                        'type' => 'strokes',
                        'score' => $request->score,
                        'hole' => $request->hole,
                        'created_at' => date('Y-m-d H:i:s')  
                    ];   
        
                    $data = DB::table('match_histroy')->insertGetId($crud);
                    
                    return response()->json(['status' => 200, 'message' => 'User Score Update Successfully','data' => $crud]);
                }
                else
                {
                    return response()->json(['status' => 200, 'message' => 'No Score Update Successfully']);
                }
            }
            else
            {
                $crud = [
                    'putts' => $request->score,
                    'hole' => $request->hole,
                    'updated_at' => date('Y-m-d H:i:s')
                    ];	
                    
                $update = DB::table('match_round_user')->where(['round_id' => $request->round_id])->where(['guest_id' => $request->user_id])->update($crud);
                if($update)
                {
                     $crud = [
                        'guest_id' => $request->user_id,
                        'round_id' => $request->round_id,
                        'type' => 'putts',
                        'score' => $request->score,
                        'hole' => $request->hole,
                        'created_at' => date('Y-m-d H:i:s')  
                    ];   
        
                    $data = DB::table('match_histroy')->insertGetId($crud);
                    
                    return response()->json(['status' => 200, 'message' => 'User Score Update Successfully','data' => $crud]);
                }
                else
                {
                    return response()->json(['status' => 200, 'message' => 'No Score Update Successfully']);
                }
            }
        }

        public function list_scrore(Request $request)
        {
            $data = DB::table('match_round_user')
            ->select('match_round_user.*','c.coursename as course_name','c.zip as zip_code','m.full_name','m.email_id','g.name as guest_name')
            ->leftjoin('golfcourses as c', 'c.course_id', 'match_round_user.course_id')
            ->leftjoin('member as m', 'm.id', 'match_round_user.user_id')
            ->leftjoin('guest as g', 'g.id', 'match_round_user.guest_id')
            ->where('match_round_user.round_id', $request->round_id)
            ->orderBy('match_round_user.id' , 'desc')->get();
            if (sizeof($data))
            {
                return response()->json(['status' => 200, 'message' => 'Data Found.', 'Data' => $data]);
            }
            else
            {
                return response()->json(['status' => 404, 'message' => 'No Round Data Found.']);
            }
        }

        public function send_mail(Request $request)
        {   
             $member = DB::table('member')->where('id',$request->user_id)->first();

            $fullname =  $member->full_name;
            $whatsapp_no =  $member->whatsapp_no;
            $email = $member->email_id;
            
            $messages = $request->message;
            $email_id=['sam@smvexperts.com','kathy@smvexperts.com'];
            Mail::send('template.messagemail', ['fullname' => $fullname,'email' => $email,'messages' => $messages], function($message) use($email_id,$request){
                    $message->to($email_id);
                    $message->subject('Contactus');
            });
            
            return response()->json(['status' => 200, 'message' => 'Message Send Successfully.']);

        }
        
        
}
