<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth,DB, Mail, Validator;
use App\Models\member;
use Illuminate\Support\Str;

class MemberController extends Controller
{
    /** construct */
        public function __construct(){
            $this->middleware('permission:categories-create', ['only' => ['create']]);
            $this->middleware('permission:categories-edit', ['only' => ['edit']]);
            $this->middleware('permission:categories-view', ['only' => ['view']]);
            $this->middleware('permission:categories-delete', ['only' => ['delete']]);
        }
    /** construct */
    /** index */
        public function index(Request $request)
        {
                $data = DB::table('member')->where('member.status','active')->orderBy('member.id', 'desc')->get();
                if (sizeof($data)) {
                    return response()->json(['status' => 200, 'message' => 'Data Found.', 'Data' => $data]);
                } else {
                    return response()->json(['status' => 404, 'message' => 'No Data Found.']);
                }
          
        }
		
        

		
		public function insert(Request $request)
        {

            $totalmember = member::Where('email_id', $request->email_id)->count();
                
            if($totalmember == 0)
            {
                if($request->file('image'))
                {
                        $file = $request->file('image');
                        $filename = time() . '.' . $request->file('image')->extension();
                        $filePath = public_path() . '/images/';
                        $file->move($filePath, $filename);
                }
                else
                { 
                    $filename = '';
                }
                $password = md5($request->password);

                 //$token = random_int(100000, 999999);
                  $token = '123456';
                  
          /*Mail::send('template.registermail', ['token' => $token], function($message) use($request){
                    $message->to($request->email_id);
                    $message->subject('Register Successfully');
                }); 
*/
               

                $crud = [
                    'status' => 'active',
                    'image'=> $filename,
                    'full_name' => $request->full_name,
                    'whatsapp_no'=>$request->whatsapp_no,
                    'email_id'=>$request->email_id,
                    'password'=> $password,
                    'otp' =>  $token,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];	
                
           
           
                $last_id = member::insertGetId($crud);


                    
                return response()->json(['status' => 200 ,'message' => 'OTP Send successfully']);

        }
        else
        {
            return response()->json(['status' => 201 ,'message' => 'Email already Register !.']);

        }    
	  }

      public function changepass(Request $request)
      {     
            $password = md5($request->password);
            $crud = [
                'password' => $password,
                'updated_at' => date('Y-m-d H:i:s')
            ];	
        
            $update = member::where(['email_id' => $request->email_id])->update($crud);
            if($update)
            {
                return response()->json(['status' => 200, 'message' => 'Password Update Successfully']);
            }
            else
            {
                return response()->json(['status' => 201, 'message' => 'Faild to update password']);
            }

      }


      public function login(Request $request){
		
		$email_id=$request->email_id;
		$password=$request->password;
        $password = md5($request->password);

        $member =  DB::table('member')->select('member.*')->where('member.status','active')
                 ->where('member.email_id',$email_id)
				 ->where('member.password',$password)
                 ->orderBy('member.id', 'desc')->get();
				 
          if (sizeof($member))
         {
            return response()->json(['status' => 200, 'message' => 'Login Successfully', 'token_type' => 'Bearer', 'data' => $member ]);
        }else{
            return response()->json(['status' => 201, 'message' => 'This account has been inactive or deleted, please contact admin']);
        }

      }
		
        
        
        public function token(Request $request)
        {

                $totalmember = member::Where('email_id', $request->email_id)->count();

                if($totalmember != 0)
                {

                        $token = '123456';

                     
                        $crud = array(
                            'otp' => $token
                            );	 
                            
                        Mail::send('template.registermail', ['token' => $token], function($message) use($request){
                    $message->to($request->email_id);
                    $message->subject('Register Successfully');
                });
                                                
                       


                    $data = DB::table('member')->where('email_id', $request->email_id)->update($crud);
    
                    return response()->json(['status' => 200 ,'message' => 'OTP send successfully']);
                } 
                else 
                {
                    return response()->json(['status' => 201 ,'message' => 'Faild to OTP send.!']);
                }    
        }

        public function tokenverify(Request $request)
        {
            
                $totalmember = member::Where('email_id', $request->email_id)->where('otp', $request->otp)->count();

                if($totalmember != 0)
                {

                    $member =  DB::table('member')->select('member.*')->where('member.status','active')
                    ->where('member.email_id',$request->email_id)
                    ->orderBy('member.id', 'desc')->get();

                    $crud = array(
                        'status' => 'active',
                        );	   


                      $data = DB::table('member')->where('email_id', $request->email_id)->update($crud);


                

                    return response()->json(['status' => 200 ,'message' => 'Member token successfully', 'token_type' => 'Bearer','Member' => $member]);
                } 
                else 
                {
                    return response()->json(['status' => 201 ,'message' => 'Faild to token Record!']);
                }    
        }

        public function sociallogin(Request $request)
        {

            $totalmember = member::Where('email_id', $request->email_id)->orWhere('token', $request->token)->count();
                
            if($totalmember == 0)
            {
                if($request->file('image'))
                {
                        $file = $request->file('image');
                        $filename = time() . '.' . $request->file('image')->extension();
                        $filePath = public_path() . '/images/';
                        $file->move($filePath, $filename);
                }
                else
                { 
                    $filename = '';
                }
                $password = md5($request->password);

         

               

                $crud = [
                    'status' => 'active',
                    'image'=> $filename,
                    'full_name' => $request->full_name,
                    'whatsapp_no'=>$request->whatsapp_no,
                    'email_id'=>$request->email_id,
                    'password'=> $password,
                    'token' =>  $request->token,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];	
                 $last_id = member::insertGetId($crud);

$member =  DB::table('member')->select('member.*')->where('member.status','active')
                 ->where('member.email_id',$request->email_id)
				 ->orderBy('member.id', 'desc')->get();
              
                    
                return response()->json(['status' => 200 ,'message' => 'Social login Register successfully.','data' => $member]);

        }
        else
        {
            $member =  DB::table('member')->select('member.*')->where('member.status','active')
                 ->where('member.email_id',$request->email_id)
				 ->orderBy('member.id', 'desc')->get();

            return response()->json(['status' => 200 ,'message' => 'Social login You  already register Register','data' => $member]);

        }    
	  }






}
