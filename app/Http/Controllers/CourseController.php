<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\course;
use App\Exports\ExportDepartment;
use Maatwebsite\Excel\Facades\Excel;
use Auth, DB, Mail, Validator, File, DataTables, Exception;
use App\Imports\ImportDepartment;

class CourseController extends Controller
{
    public function __construct(){
        /*$this->middleware('permission:city-create', ['only' => ['create']]);
        $this->middleware('permission:city-edit', ['only' => ['edit']]);
        $this->middleware('permission:city-view', ['only' => ['view']]);
        $this->middleware('permission:city-delete', ['only' => ['delete']]);*/
    }
/** construct */

/** index */
    public function index(Request $request){
        if($request->ajax()){
            $data = course::select('*')->orderBy('id' , 'desc')->get();
            
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($data){
                        $return = '<div class="btn-group">';

                        
                     /*   if(auth()->user()->can('city-edit')){*/
                 /*           $return .= '<a href="'.route('course.edit',['id' => base64_encode($data->id)]).'" class="btn btn-default btn-xs">
                                            <i class="fa fa-pencil"></i>
                                        </a> &nbsp;'; */
                     //   }   

                       //if (auth()->user()->can('city-delete')) {
                            $return .= '<a href="javascript:;" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-bars"></i>
                                            </a> &nbsp;
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" href="javascript:;" onclick="change_status(this);" data-status="deleted" data-id="' . base64_encode($data->id) . '">Delete</a></li>
                                            </ul>';
                       // }

                        $return .= '</div>';

                        return $return;
                    })

                
                    ->editColumn('status', function ($data) {
                        if ($data->status == 'active') {
                            return '<span class="badge badge-pill badge-success">Active</span>';
                        } else if ($data->status == 'inactive') {
                            return '<span class="badge badge-pill badge-warning">Inactive</span>';
                        } else if ($data->status == 'deleted') {
                            return '<span class="badge badge-pill badge-danger">Deleted</span>';
                        }else{
                            return '-';
                        }
                    })

                    ->rawColumns([ 'action' ,'status'])
                    ->make(true);
            }
        return view('course.index');
    }
/** index */

/** create */
 public function create(Request $request){
        
        $data = course::select('course.*')->where(['status' => 'active'])->get(); 
        return view('course.create')->with(['data' => $data]);
    }


   public function insert(Request $request){
        if($request->ajax()){
            return true ;
        }
        DB::table('golfholes')->where(['course_zip'=> $request->zip_code])->delete();
        DB::table('landmarks')->where(['CourseZip'=> $request->zip_code])->delete();
        DB::table('course')->where(['zip_code'=> $request->zip_code])->delete();
        

        if($request->file('file'))
        {
           /* $file = $request->file('file');
            $content= File::get($file->getRealPath());
            $filename = time() . '.' . $request->file('file')->extension();
            $filePath = public_path() . '/images/';
            $file->move($filePath, $filename); */
            

        }
        else
        {
            $filename = '';
        } 
      
      
      
        $landmarkfile = "https://golftraxx.com/courses_by_zip/landmarks/".$request->zip_code.".txt";
        $holefile = "https://golftraxx.com/courses_by_zip/holes/".$request->zip_code.".txt";
        $wireframe_hubspoke="https://golftraxx.com/courses_by_zip/wireframe_hubspoke/".$request->zip_code.".txt";

         
        $fp = fopen($holefile, "r");
        $course_name_list=array();
       
        while(!feof($fp))
        {
            $line = fgets($fp);
            $line = str_replace("\t\t","\t",$line);
            $holefileArray = explode("\t",$line);
            if(count($holefileArray) == 35)
            {
              
               

                if (!in_array($holefileArray[1], $course_name_list)) {
                    array_push($course_name_list,$holefileArray[1]);

                }
            
 
              $insertland = [
                'course_id' =>  $holefileArray[0],
                'course_name' =>  $holefileArray[1],
                'course_zip' =>  $holefileArray[2],
                'course_phone' =>  $holefileArray[3],
                'hole' =>  $holefileArray[4],
                'pro_tee' =>  $holefileArray[5],
                'pro_par' =>  $holefileArray[6],
                'pro_yards' =>  $holefileArray[7],
                'pro_HCP' =>  $holefileArray[8],
                'champ_tee' =>  $holefileArray[9],
                'champ_par' =>  $holefileArray[10],
                'champ_yards' =>  $holefileArray[11],
                'champ_HCP' =>  $holefileArray[12],
                'mens_tee' =>  $holefileArray[13],
                'mens_par' =>  $holefileArray[14],
                'mens_yards' =>  $holefileArray[15],
                'mens_HCP' =>  $holefileArray[16],
                'womens_tee' =>  $holefileArray[17],
                'womens_par' =>  $holefileArray[18],
                'womens_yards' =>  $holefileArray[19],
                'womens_HCP' =>  $holefileArray[20],
                'juniors_tee' =>  $holefileArray[21],
                'juniors_par' =>  $holefileArray[22],
                'juniors_yards' =>  $holefileArray[23],
                'juniors_HCP' =>  $holefileArray[24],
                'hole_tee_target_latitude' =>  $holefileArray[25],
                'hole_tee_target_longitude' =>  $holefileArray[26],
                'hole_greenfront_latitude' =>  $holefileArray[27],
                'hole_greenfront_longitude' =>  $holefileArray[28],
                'hole_greenmiddle_latitude' =>  $holefileArray[29],
                'hole_greenmiddle_longitude' =>  $holefileArray[30],
                'hole_greenback_latitude' =>  $holefileArray[31],
                'hole_greenback_longitude' =>  $holefileArray[32],
                'hole_latitude' =>  $holefileArray[33],
                'hole_longitude' =>  $holefileArray[34], 
            ];
            DB::table('golfholes')->insert($insertland);

            }   
           
        }   
        
        $fp = fopen($landmarkfile, "r");
    
        while(!feof($fp))
        {
            $line = fgets($fp);
            $line = str_replace("\t\t","\t",$line);
            $resultArray = explode("\t", $line);
                      

            if(count($resultArray) == 13)
            {
        
                $insertland2 = [
                    'key' =>  $resultArray[0],
                    'CourseName' =>  $resultArray[1],
                    'CourseZip' =>  $resultArray[2],
                    'CoursePhone' =>  $resultArray[3],
                    'Hole' =>  $resultArray[4],
                    'landmark_name' =>  $resultArray[5],
                    'hole_landmark_position' =>  $resultArray[6],
                    'hole_landmark_layup_lattitude' =>  $resultArray[7],
                    'hole_landmark_layup_longitude' =>  $resultArray[8],
                    'hole_landmark_carry_lattitude' =>  $resultArray[9],
                    'hole_landmark_carry_longitude' =>  $resultArray[10],
                    'current_lattitude' =>  $resultArray[11],
                    'current_longitude' =>  $resultArray[12],

                ];
                DB::table('landmarks')->insert($insertland2);

            }   
           
        }
        
        $fp = fopen($wireframe_hubspoke, "r");
        
        while(!feof($fp))
        {             

            $line = fgets($fp);
            $resultArray = explode("||", $line);
            if(count($resultArray) == 33)
            {  
                $hole_flyover_url= stripslashes($resultArray[32]);
                $insertlandd = [
                    'key' =>  $resultArray[0],
                    'course_name' =>  $resultArray[1],
                    'address' =>  $resultArray[2],
                    'course_zip' =>  $resultArray[3],
                    'hole' =>  $resultArray[4],
                    'hole_no' =>  $resultArray[5],
                    'hole_flyover_url' =>  $resultArray[32]
                ];
            
                DB::table('wireframe_hubspoke')->insert($insertlandd);
            }   
        
        }   

        
        

        $course_name = implode(",",$course_name_list);

        $crud = [
            'course_name' => $course_name,
            'zip_code' =>  $request->zip_code,
            /*'lat' =>  $request->lat,
            'long' =>  $request->long,
            'file' =>  $filename, */
            'status' => 'active',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),

        ];
        
        DB::beginTransaction();
        try {
            DB::enableQueryLog();
            $last_id = course::insertGetId($crud);
            if ($last_id) {
                
                DB::commit();
                return redirect()->route('course')->with('success', 'Record inserted successfully');
            } else {
                DB::rollback();
                return redirect()->back()->with('error', 'Failed to insert record')->withInput();
            }
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
        }
    }
/** insert */

/** view */
    public function view(Request $request){  
        $id = base64_decode($request->id);
                                
        $data = course::select('*')->where(['id' => $id])->first();
        return view('course.view')->with(['data' => $data]);
    }
/** view */

/** edit */
    public function edit(Request $request){
        $id = base64_decode($request->id);
                                
        $data = course::select('*')->where(['id' => $id])->first();
         
       return view('course.edit')->with(['data' => $data]);
    }
/** edit */

/** update */
    public function update(Request $request){
        if($request->ajax()){
            return true ;
        }
       // $branch_admin=$request->branch_admin ? 1 : NULL;

       if($request->file('file'))
       {
           $file = $request->file('file');
           $filename = time() . '.' . $request->file('file')->extension();
           $filePath =  public_path() . '/images/';
           $file->move($filePath, $filename);
           
       }
       else
       {
           $filename = $request->oldlogo;
       }

        $crud = [
            'course_name' => $request->course_name,
            'zip_code' =>  $request->zip_code,
            'lat' =>  $request->lat,
            'long' =>  $request->long,
            'file' =>  $filename,
            'total_hole' =>  $request->total_hole,
            'status' => 'active',
            'updated_at' => date('Y-m-d H:i:s'),
    
        ];
       
        
        DB::beginTransaction();
        try {
            DB::enableQueryLog();
            $update = course::where(['id' => $request->id])->update($crud);
            if($update){
                DB::commit();
                return redirect()->route('course')->with('success', 'Record updated successfully');
            } else {
                DB::rollback();
                return redirect()->back()->with('error', 'Failed to update record')->withInput();
            }
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error', 'Something went wrong, please try again later')->withInput();
        }
    }
/** update */

/** change-status */
    public function change_status(Request $request){
        if (!$request->ajax()) { exit('No direct script access allowed'); }
            
            $id = base64_decode($request->id);
            $data = course::where(['id' => $id])->first();
       
            if(!empty($data)){
                $update = course::where(['id' => $id])->update(['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')]);
               
                DB::table('golfholes')->where(['course_zip'=> $data->zip_code])->delete();
                DB::table('landmarks')->where(['CourseZip'=> $data->zip_code])->delete();
                DB::table('course')->where(['zip_code'=> $data->zip_code])->delete();
                DB::table('wireframe_hubspoke')->where(['course_zip'=> $request->zip_code])->delete();

                
                if($update){
                    return response()->json(['code' => 200]);
                }else{
                    return response()->json(['code' => 201]);
                }
            }else{
                return response()->json(['code' => 201]);
            }
    }


}

