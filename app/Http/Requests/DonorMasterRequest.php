<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DonorMasterRequest extends FormRequest{
    public function authorize(){
        return true;
    }

    public function rules(){
        if($this->method() == 'PATCH'){
            return [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|email',
                'mobile_number' => 'required|digits:10|unique:donor_master,mobile_number,'.$this->id,
                'pancard' => 'required',
                'address' => 'required'
            ];
        }else{
            return [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|email',
                'mobile_number' => 'required|digits:10|unique:donor_master,mobile_number',
                'pancard' => 'required',
                'address' => 'required'
            ];
        }
    }

    public function messages(){
        return [
            'first_name.required' => 'Please enter first name',
            'first_name.max' => 'Please enter first name maximum 255 characters',
            'last_name.required' => 'Please enter last name',
            'last_name.max' => 'Please enter last name maximum 255 characters',
            'email.required' => 'Please enter email',
            'email.email' => 'Please enter valid email',
            'mobile_number.required' => 'Please enter mobile number',
            'mobile_number.digits' => 'Please enter 10 digit number',
            'mobile_number.unique' => 'Please enter unique mobile number',
            'pancard.required' => 'Please select pancard',
            'address.required' => 'Please select address'
        ];
    }
}
