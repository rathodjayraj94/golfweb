<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_supplier', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('comp_type')->nullable();
            $table->text('comp_address')->nullable();
            $table->string('email')->nullable();          
            $table->string('mobile_no')->nullable();
            $table->string('work_phone')->nullable();
            $table->string('whatsapp_no')->nullable();
            $table->string('website')->nullable();
            $table->integer('countries_id')->nullable();
            $table->integer('cities_id')->nullable();
            $table->integer('states_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('sub_category_id')->nullable();
            $table->integer('doe')->nullable();
            $table->integer('busines_package')->nullable();
            $table->integer('busines_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_supplier');
    }
}
