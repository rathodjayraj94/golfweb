<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_product', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id')->nullable();
            $table->integer('sub_category_id')->nullable();
            $table->string('name')->nullable();
            $table->string('size')->nullable();
            $table->string('usage')->nullable();
            $table->integer('counsuption_countries_id')->nullable();
            $table->integer('counsuption_cities_id')->nullable();
            $table->integer('counsuption_states_id')->nullable();
            $table->integer('production_countries_id')->nullable();
            $table->integer('production_cities_id')->nullable();
            $table->integer('production_states_id')->nullable();
            $table->string('product_title')->nullable();
            $table->string('f_s_s_r_no')->nullable();
            $table->string('production_code')->nullable();
            $table->string('h_s_n_code')->nullable();
            $table->text('description')->nullable();
            $table->enum('qa_check', ['Yes', 'No'])->default('Yes');
            $table->enum('natural_artificial', ['Natural', 'Artificial'])->default('Natural');
            $table->enum('homemade_machine', ['Homemade', 'Machine'])->default('Homemade');
            $table->string('tax')->nullable();
            $table->string('certification')->nullable();
            $table->string('top_ten_production_countries')->nullable();
            $table->string('top_ten_counsuption_countries')->nullable();
            $table->string('top_ten_suplier')->nullable();
            $table->string('top_ten_buyer')->nullable();
            $table->string('product_info_link_youtube')->nullable();
            $table->enum('status', ['active', 'inactive','deleted' ,'pdi_hold'])->default('pdi_hold');
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_product');
    }
}
